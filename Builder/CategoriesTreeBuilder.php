<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Builder;

use Nfq\Bundle\TecDocBundle\Entity\Category;
use Nfq\Bundle\TecDocBundle\Entity\Collections\CategoriesTree;
use Nfq\Bundle\TecDocBundle\Generator\CategorySlugGeneratorInterface;
use Nfq\Bundle\TecDocBundle\Generator\RootCategoryGeneratorInterface;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;

class CategoriesTreeBuilder
{
    /**
     * @var CategorySlugGeneratorInterface
     */
    private $slugGenerator;

    /**
     * @var RootCategoryGeneratorInterface
     */
    private $rootCategoryGenerator;

    /**
     * @param CategorySlugGeneratorInterface $slugGenerator
     * @param RootCategoryGeneratorInterface $rootCategoryGenerator
     */
    public function __construct(
        CategorySlugGeneratorInterface $slugGenerator,
        RootCategoryGeneratorInterface $rootCategoryGenerator
    ) {
        $this->slugGenerator = $slugGenerator;
        $this->rootCategoryGenerator = $rootCategoryGenerator;
    }

    /**
     * @param \stdClass[] $nodes
     * @return CategoriesTree
     */
    public function buildCategoriesTree(array $nodes): CategoriesTree
    {
        $categories = $this->convertNodesToCategories($nodes);
        $categories = $this->assignRootCategoryParentId($this->rootCategoryGenerator->generate(), $categories);
        $this->assignChildren($categories);
        // Filter out categories with parents.
        $categories = $this->removeNonRootCategories($categories);
        $this->assignLevel($categories);

        return new CategoriesTree($categories);
    }

    /**
     * @param \stdClass[] $nodes
     * @return Category[]
     */
    protected function convertNodesToCategories(array $nodes): array
    {
        $categories = [];
        foreach ($nodes as $node) {
            $category = Category::createFromTecDocItem($node);
            $category->setSlug($this->slugGenerator->getSlug($category));
            $categories[$category->getId()] = $category;
        }

        return $categories;
    }

    /**
     * @param Category $rootCategory
     * @param Category[] $categories
     * @return Category[]
     */
    protected function assignRootCategoryParentId(Category $rootCategory, array $categories): array
    {
        foreach ($categories as $category) {
            if (null === $category->getParentId()) {
                $category->setParentId($rootCategory->getId());
            }
        }

        $categories[$rootCategory->getId()] = $rootCategory;

        return $categories;
    }

    /**
     * @param Category[] $categories
     */
    protected function assignChildren(array $categories): void
    {
        foreach ($categories as $category) {
            if (null === $parentId = $category->getParentId()) {
                continue;
            }

            if (!Arr::keyExists($parentId, $categories)) {
                continue;
            }
            /** @var Category $parent */
            $parent = $categories[$parentId];
            $parent->addChild($category);
        }
    }

    /**
     * @param Category[] $categories
     * @return Category[]
     */
    protected function removeNonRootCategories(array $categories): array
    {
        return \array_filter(
            $categories,
            static function (Category $category): bool {
                return $category->isRoot();
            }
        );
    }

    /**
     * @param Category[] $categories
     */
    protected function assignLevel(array $categories): void
    {
        foreach ($categories as $category) {
            if ($category->isRoot()) {
                $category->setLevel(0);
            }
        }
    }
}
