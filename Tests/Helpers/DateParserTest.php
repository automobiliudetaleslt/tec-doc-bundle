<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Helpers;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Helpers\DateParser;

class DateParserTest extends TestCase
{
    /**
     * @dataProvider tecDocStringDateProvider
     * @param string $date
     * @param string $expectedOutput
     */
    public function testShouldParseTecDocStringDateFormat(string $date, string $expectedOutput)
    {
        $output = DateParser::getDateTime($date);

        $this->assertInstanceOf(\DateTime::class, $output);

        $this->assertEquals($expectedOutput, $output->format('Y-m'));
    }

    /**
     * @dataProvider tecDocStringDateProvider
     * @param string $date
     * @param string $expectedOutput
     */
    public function testDateParsing(string $date, string $expectedOutput)
    {
        $output = DateParser::parseDate($date);

        $this->assertEquals($expectedOutput, $output);
    }

    /**
     * @return array
     */
    public function tecDocStringDateProvider()
    {
        return [
            [
                '196604',
                '1966-04',
            ],
            [
                '197510',
                '1975-10',
            ],
        ];
    }
}
