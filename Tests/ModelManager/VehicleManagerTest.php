<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Resolver\VehicleTargetTypeResolver;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;

class VehicleManagerTest extends TestCase
{
    /**
     * @var TecDocApiManagerInterface|MockObject
     */
    protected $apiManager;

    /**
     * @var VehicleTargetTypeResolver|MockObject
     */
    protected $vehicleTargetTypeResolver;

    /**
     * @var VehicleManager;
     */
    protected $manager;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->apiManager = $this->createMock(TecDocApiManagerInterface::class);
        $this->vehicleTargetTypeResolver = $this->createMock(VehicleTargetTypeResolver::class);
        $this->manager = new VehicleManager($this->apiManager, $this->vehicleTargetTypeResolver);
    }

    /**
     * @dataProvider manufacturerDataProvider
     * @param array $manufacturerData
     * @param int $manufacturersCount
     */
    public function testGeArticleCompatibleVehicleManufacturers(array $manufacturerData, int $manufacturersCount)
    {
        $tecDocManufacturers = [];
        foreach ($manufacturerData as $item) {
            $tecDocManufacturers[] = $this->createTecDocManufacturer($item['id'], $item['name']);
        }

        $this->apiManager->method('getArticleLinkedAllLinkingTargetManufacturer2')
            ->with(1)->willReturn($tecDocManufacturers);

        $manufacturers = $this->manager->geArticleCompatibleVehicleManufacturers(1);

        $this->assertCount($manufacturersCount, $manufacturers);

        foreach ($manufacturers as $manufacturer) {
            $this->assertInstanceOf(VehicleManufacturer::class, $manufacturer);
            $manufacturerId = $manufacturer->getId();
            $manufacturerTitle = $manufacturer->getTitle();

            foreach ($manufacturerData as $manufacturerDataItem) {
                if ($manufacturerDataItem['id'] === $manufacturerId
                    && $manufacturerDataItem['name'] === $manufacturerTitle
                ) {
                    continue 2;
                }
            }

            $this->fail(\sprintf('Manufacturer id %s, title %s not found', $manufacturerId, $manufacturerTitle));
        }
    }

    /**
     * @return array
     */
    public function manufacturerDataProvider()
    {
        return [
            [
                [
                    [
                        'id' => 1526,
                        'name' => 'INFINITI',
                    ],
                    [
                        'id' => 80,
                        'name' => 'NISSAN',
                    ],
                    [
                        'id' => 93,
                        'name' => 'RENAULT',
                    ],
                ],
                3,
            ],
            [
                [
                    [
                        'id' => 1526,
                        'name' => 'INFINITI',
                    ],
                    [
                        'id' => 80,
                        'name' => 'NISSAN',
                    ],
                    [
                        'id' => 80,
                        'name' => 'NISSAN',
                    ],
                ],
                2,
            ],
            [
                [],
                0,
            ],
        ];
    }

    /**
     * @test
     */
    public function testGgetArticleCompatibleVehicles()
    {
        $this
            ->apiManager
            ->method('getArticleLinkedAllLinkingTarget4')
            ->willReturn($this->creteTecDocArticleLinkages());

        $this
            ->apiManager
            ->method('getArticleLinkedAllLinkingTargetsByIds3')
            ->willReturn($this->createTecDocLinkTarget());

        /** @var Vehicle[] $vehicles */
        $vehicles = $this->manager->getArticleCompatibleVehicles(123, '123');
        $vehicle = \reset($vehicles);

        $this->assertCount(1, $vehicles);
        $this->assertInstanceOf(Vehicle::class, $vehicle);

        $productAttributes = $vehicle->getProductAttributes();

        $this->assertCount(1, $productAttributes);
        $productAttribute = \reset($productAttributes);
        $this->assertInstanceOf(Attribute::class, $productAttribute);
    }

    /**
     * @test
     */
    public function testGetVehicleManufacturers()
    {
        $manufacturer1 = $this->createTecDocManufacturer(0, '0');
        $manufacturer2 = $this->createTecDocManufacturer(1, '1');
        $manufacturer3 = $this->createTecDocManufacturer(2, '2');
        $manufacturer4 = $this->createTecDocManufacturer(3, '3');
        $manufacturer5 = $this->createTecDocManufacturer(4, 'BMW motorcycle');

        $this->apiManager->method('getManufacturers')->willReturn(
            [
                $manufacturer1,
                $manufacturer2,
                $manufacturer3,
                $manufacturer4,
                $manufacturer5,
            ]
        );

        $vehicleManufacturers = $this->manager->getVehicleManufacturers();

        $this->assertCount(4, $vehicleManufacturers);

        foreach ($vehicleManufacturers as $key => $vehicleManufacturer) {
            $this->assertEquals($key, $vehicleManufacturer->getTitle());
            $this->assertEquals($key, $vehicleManufacturer->getId());
        }

        $motorcycleManufacturers = $this->manager->getVehicleManufacturers(VehicleManager::TYPE_MOTORCYCLE);

        $this->assertCount(1, $motorcycleManufacturers);
        $this->assertEquals(4, $motorcycleManufacturers[4]->getId());
        $this->assertEquals('BMW motorcycle', $motorcycleManufacturers[4]->getTitle());
    }

    /**
     * @test
     */
    public function testGetVehicleModels()
    {
        $tecDocModelItem1 = new \stdClass();
        $tecDocModelItem1->modelId = 0;
        $tecDocModelItem1->modelname = '02 (E10) M';
        $tecDocModelItem1->yearOfConstrFrom = 196604;
        $tecDocModelItem1->yearOfConstrTo = 197707;

        $tecDocModelItem2 = new \stdClass();
        $tecDocModelItem2->modelId = 1;
        $tecDocModelItem2->modelname = '02 (E10) M';
        $tecDocModelItem2->yearOfConstrFrom = 196604;
        $tecDocModelItem2->yearOfConstrTo = 197707;

        $models = [
            $tecDocModelItem1,
            $tecDocModelItem2,
        ];

        $this->apiManager->method('getModelSeries')->willReturn($models);

        $vehicleModels = $this->manager->getVehicleModels(1, 1);
        $this->assertCount(2, $vehicleModels);

        foreach ($vehicleModels as $key => $vehicleModel) {
            $this->assertTrue($vehicleModel instanceof VehicleModel);
            $this->assertEquals($key, $vehicleModel->getId());
        }
    }

    /**
     * @test
     */
    public function testGetVehicles()
    {
        $vehicleData1 = new \stdClass();
        $vehicleData1->carId = 1;
        $vehicleData2 = new \stdClass();
        $vehicleData2->carId = 2;

        $this->apiManager->method('getVehicleIdsByCriteria')->willReturn(
            [
                $vehicleData1,
                $vehicleData2,
            ]
        );

        $vehicleData = [
            'carId' => 108002,
            'vehicleDetails' => [
                'carId' => 108002,
                'ccmTech' => 2979,
                'constructionType' => 'uždaryta visureigė transporto priemonė',
                'cylinder' => 6,
                'cylinderCapacityCcm' => 2979,
                'cylinderCapacityLiter' => 300,
                'fuelType' => 'benzinas',
                'fuelTypeProcess' => 'tiesioginis įpurškimas',
                'impulsionType' => 'visų ratų pavara',
                'manuId' => 16,
                'manuName' => 'BMW',
                'modId' => 12845,
                'modelName' => '(F16, F86)',
                'motorType' => 'benzininis variklis',
                'powerHpFrom' => 306,
                'powerHpTo' => 306,
                'powerKwFrom' => 225,
                'powerKwTo' => 225,
                'typeName' => 'xDrive 35 i',
                'typeNumber' => 108002,
                'valves' => 4,
                'yearOfConstrFrom' => 201412,
            ],
        ];

        $vehicleData = \json_decode(
            \json_encode($vehicleData, \JSON_THROW_ON_ERROR, 512),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $this->apiManager->method('getVehicleByIds4')->willReturn(
            [
                $vehicleData,
            ]
        );

        $vehicles = $this->manager->getVehicles(1, 2);

        $this->assertCount(1, $vehicles);
        $vehicle = $vehicles[0];

        $this->assertInstanceOf(Vehicle::class, $vehicle);
        $this->assertEquals(108002, $vehicle->getId());
        $this->assertEquals('xDrive 35 i', $vehicle->getDescription());
    }

    /**
     * @return array
     */
    protected function createTecDocLinkTarget(): array
    {
        $vehicle = new \stdClass();
        $vehicle->carDesc = '35 visų ratų pavara';
        $vehicle->carId = 18811;
        $vehicle->constructionType = 'uždaryta visureigė transporto priemonė';
        $vehicle->cylinderCapacity = 3498;
        $vehicle->linkingTargetType = 'P';
        $vehicle->manuDesc = 'INFINITI';
        $vehicle->manuId = 1526;
        $vehicle->modelDesc = 'FX';
        $vehicle->modelId = 5441;
        $vehicle->powerHpFrom = 280;
        $vehicle->powerHpTo = 280;
        $vehicle->powerKwFrom = 206;
        $vehicle->powerKwTo = 206;
        $vehicle->yearOfConstructionFrom = 200301;
        $vehicle->yearOfConstructionTo = 200812;

        $link = new \stdClass();
        $link->array = [
            $vehicle,
        ];

        $productAttribtues = [
            $this->createProductAttribtue(),
        ];

        $array = new \stdClass();
        $array->array = $productAttribtues;

        $targetLink = new \stdClass();
        $targetLink->linkedVehicles = $link;
        $targetLink->linkedArticleImmediateAttributs = $array;

        return [
            $targetLink,
        ];
    }

    /**
     * @return \stdClass
     */
    protected function createProductAttribtue(): \stdClass
    {
        $productAttribute = new \stdClass();
        $productAttribute->attrBlockNo = -1;
        $productAttribute->attrId = 100;
        $productAttribute->attrIsConditional = false;
        $productAttribute->attrIsInterval = false;
        $productAttribute->attrName = 'montavimo vieta';
        $productAttribute->attrShortName = 'montavimo vieta';
        $productAttribute->attrType = 'K';
        $productAttribute->attrValue = 'priekinė ašis';

        return $productAttribute;
    }

    /**
     * @return array
     */
    protected function creteTecDocArticleLinkages(): array
    {
        $articleLinkage = new \stdClass();
        $articleLinkage->articleLinkId = 123;
        $articleLinkage->linkingTargetId = 123;

        $articleLinkages = new \stdClass();
        $articleLinkages->array = [
            $articleLinkage,
        ];

        $tecDocArticleLinkages = new \stdClass();
        $tecDocArticleLinkages->articleLinkages = $articleLinkages;


        return [
            $tecDocArticleLinkages,
        ];
    }

    /**
     * @param int $id
     * @param string $name
     * @return \stdClass
     */
    protected function createTecDocManufacturer(int $id, string $name): \stdClass
    {
        $tecDocManufacturer = new \stdClass();
        $tecDocManufacturer->manuId = $id;
        $tecDocManufacturer->manuName = $name;

        return $tecDocManufacturer;
    }
}
