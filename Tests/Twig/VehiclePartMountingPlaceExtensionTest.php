<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Twig;

use Nfq\Bundle\TecDocBundle\Provider\MountingPlaceProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Twig\VehiclePartMountingPlaceExtension;

class VehiclePartMountingPlaceExtensionTest extends TestCase
{
    /**
     * @var MountingPlaceProvider|MockObject
     */
    protected $mountingPlaceProvider;

    /**
     * @var VehiclePartMountingPlaceExtension
     */
    protected $twigExtension;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->mountingPlaceProvider = $this->createMock(MountingPlaceProvider::class);

        $this->twigExtension = new VehiclePartMountingPlaceExtension($this->mountingPlaceProvider);
    }

    /**
     * @test
     */
    public function testGetMountingPlace()
    {
        $mountingPlace = 'string';

        $this->mountingPlaceProvider->method('getMountingPlace')->willReturn($mountingPlace);

        $this->assertEquals($mountingPlace, $this->twigExtension->getMountingPlace('bar'));
    }
}
