<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Provider;

use Nfq\Bundle\TecDocBundle\Provider\LocaleProvider;
use Nfq\Bundle\TecDocBundle\Provider\MountingPlaceProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Provider\MountingPlaceTableProviderInterface;
use Nfq\Bundle\TecDocBundle\Model\PartMountingPlaceTable;

class MountingPlaceProviderTest extends TestCase
{
    /**
     * @var MountingPlaceProvider
     */
    protected $mountingPlaceProvider;

    /**
     * @var LocaleProvider|MockObject
     */
    protected $localeProvider;

    /**
     * @var MountingPlaceTableProviderInterface|MockObject
     */
    protected $mountingPlaceTableProvider;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->localeProvider = $this->createMock(LocaleProvider::class);
        $this->mountingPlaceTableProvider = $this->createMock(MountingPlaceTableProviderInterface::class);

        $this->mountingPlaceProvider = new MountingPlaceProvider(
            $this->localeProvider,
            $this->mountingPlaceTableProvider
        );
    }

    /**
     * @test
     */
    public function testGetMountingPlace()
    {
        $mountingPlaceTable = new PartMountingPlaceTable();
        $mountingPlaceTable->setMountingPlace('foo', 'bar', 'baz');

        $this->localeProvider
            ->expects($this->once())
            ->method('getLocale')
            ->willReturn('foo');

        $this
            ->mountingPlaceTableProvider
            ->expects($this->once())
            ->method('getPartMountingPlaceTable')
            ->willReturn($mountingPlaceTable);

        $mountingPlace = $this->mountingPlaceProvider->getMountingPlace('bar');

        $this->assertEquals('baz', $mountingPlace);
    }

    /**
     * @test
     */
    public function testGetMountingPlaceOnMultipleCalls()
    {
        $mountingPlaceTable = new PartMountingPlaceTable();
        $mountingPlaceTable->setMountingPlace('foo', 'bar', 'baz');

        $this->localeProvider
            ->expects($this->exactly(3))
            ->method('getLocale')
            ->willReturn('foo');

        $this
            ->mountingPlaceTableProvider
            ->expects($this->once())
            ->method('getPartMountingPlaceTable')
            ->willReturn($mountingPlaceTable);

        $this->assertEquals('baz', $this->mountingPlaceProvider->getMountingPlace('bar'));
        $this->assertEquals('baz', $this->mountingPlaceProvider->getMountingPlace('bar'));
        $this->assertEquals('baz', $this->mountingPlaceProvider->getMountingPlace('bar'));
    }

    /**
     * @test
     */
    public function testGetNonExistingMountingPlace()
    {
        $mountingPlaceTable = new PartMountingPlaceTable();

        $this->localeProvider
            ->expects($this->once())
            ->method('getLocale')
            ->willReturn('foo');

        $this
            ->mountingPlaceTableProvider
            ->expects($this->once())
            ->method('getPartMountingPlaceTable')
            ->willReturn($mountingPlaceTable);

        $this->assertEquals(null, $this->mountingPlaceProvider->getMountingPlace('bar'));
    }
}
