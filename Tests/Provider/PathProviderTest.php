<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Provider;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Provider\PathProvider;
use Symfony\Component\Routing\RouterInterface;

class PathProviderTest extends TestCase
{
    /**
     * @var PathProvider
     */
    protected $pathProvider;

    /**
     * @var RouterInterface|MockObject
     */
    protected $router;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->router = $this->createMock(RouterInterface::class);

        $this->pathProvider = new PathProvider(
            'http://webservice-cs.tecdoc.net/pegasus-3-0/documents',
            10,
            $this->router
        );
    }

    /**
     * @test
     */
    public function testDocumentPath()
    {
        $this->assertEquals(
            'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/10/1/0',
            $this->pathProvider->getDocumentPath(1)
        );
    }

    /**
     * @test
     */
    public function testThumbnailPath()
    {
        $this->assertEquals(
            'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/10/1/2',
            $this->pathProvider->getThumbnailPath(1, 2)
        );
    }

    /**
     * @test
     */
    public function testInternalThumbnailPath()
    {
        $this->router->method('generate')->willReturn('mockedRoute');

        $this->assertEquals(
            'mockedRoute',
            $this->pathProvider->getInternalThumbnailPath(1, 1)
        );
    }
}
