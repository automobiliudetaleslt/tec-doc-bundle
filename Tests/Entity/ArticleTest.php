<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use Nfq\Bundle\TecDocBundle\Exception\FieldNotFoundException;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;
use Nfq\Bundle\TecDocBundle\Entity\Document;
use Nfq\Bundle\TecDocBundle\Entity\OeNumber;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Entity\Thumbnail;

class ArticleTest extends TestCase
{
    /**
     * @var Article
     */
    protected $product;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->product = new Article();
    }

    /**
     * @test
     */
    public function testOeNumbers()
    {
        $oeNumber = new OeNumber();

        $this->product->addOeNumber($oeNumber);

        $oeNumbers = $this->product->getOeNumbers();

        $this->assertCount(1, $oeNumbers);
        $this->assertEquals($oeNumber, $oeNumbers[0]);
    }

    /**
     * @test
     */
    public function testHasOeNumbers()
    {
        $oeNumber = new OeNumber();

        $this->product->addOeNumber($oeNumber);

        $this->assertTrue($this->product->hasOeNumbers());
    }

    /**
     * @test
     */
    public function testDocuments()
    {
        $document = new Document();

        $this->product->addDocument($document);

        $documents = $this->product->getDocuments();

        $this->assertCount(1, $documents);
        $this->assertEquals($document, $documents[0]);
    }

    /**
     * @test
     */
    public function testHasDocuments()
    {
        $document = new Document();

        $this->product->addDocument($document);

        $this->assertTrue($this->product->hasDocuments());
    }

    /**
     * @test
     */
    public function testThumbnails()
    {
        $this->assertCount(0, $this->product->getThumbnails());

        $this->product->addThumbnail(new Thumbnail(1, '2', 3));

        $this->assertCount(1, $this->product->getThumbnails());
    }

    /**
     * @test
     */
    public function testHasThumbNails()
    {
        $this->assertFalse($this->product->hasThumbnails());

        $this->product->addThumbnail(new Thumbnail(1, '2', 3));

        $this->assertTrue($this->product->hasThumbnails());
    }

    /**
     * @test
     * @throws FieldNotFoundException
     */
    public function testArticleCreationFromTecDocItem()
    {
        $tecDocArticleItem = $this->getTecDocArticleItem();

        $article = Article::createFromTecDocItem($tecDocArticleItem);

        $this->assertInstanceOf(Article::class, $article);

        $this->assertEquals(3211264, $article->getId());
        $this->assertEquals('Stabdžių trinkelių rinkinys, diskinis stabdys', $article->getName());
        $this->assertEquals(37791, $article->getNumber());
        $this->assertEquals('A.B.S.', $article->getBrandName());
        $this->assertEquals(206, $article->getBrandNumber());

        $thumbnails = $article->getThumbnails();
        $attributes = $article->getAttributes();
        $documents = $article->getDocuments();
        $oeNumbers = $article->getOeNumbers();

        foreach ($thumbnails as $thumbnail) {
            $this->assertInstanceOf(Thumbnail::class, $thumbnail);
        }

        foreach ($attributes as $attribute) {
            $this->assertInstanceOf(Attribute::class, $attribute);
        }

        foreach ($documents as $document) {
            $this->assertInstanceOf(Document::class, $document);
        }

        foreach ($oeNumbers as $oeNumber) {
            $this->assertInstanceOf(OeNumber::class, $oeNumber);
        }

        $this->assertCount(2, $thumbnails);
        $this->assertCount(2, $attributes);
        $this->assertCount(1, $documents);
        $this->assertCount(2, $oeNumbers);

        $this->assertFalse($article->hasEquipment());
        $this->assertFalse($article->hasAccessories());
    }

    /**
     * @return \stdClass
     */
    protected function getTecDocArticleItem()
    {
        $item = [
            'articleAttributes' => [
                'array' => [
                    0 => [
                        'attrBlockNo' => 0,
                        'attrId' => 649,
                        'attrIsConditional' => false,
                        'attrIsInterval' => false,
                        'attrIsLinked' => false,
                        'attrName' => 'stabdžių sistema',
                        'attrShortName' => 'stabdžių sistema',
                        'attrType' => 'A',
                        'attrValue' => 'SUMITOMO',
                        'attrValueId' => 12468566,
                    ],
                    1 => [
                        'attrBlockNo' => 0,
                        'attrId' => 1168,
                        'attrIsConditional' => false,
                        'attrIsInterval' => false,
                        'attrIsLinked' => false,
                        'attrName' => 'storis / stirpumas 1 [mm]',
                        'attrShortName' => 'storis / stiprumas 1',
                        'attrType' => 'N',
                        'attrUnit' => 'mm',
                        'attrValue' => 16,
                        'attrValueId' => 12468567,
                    ],
                ],
            ],
            'articleDocuments' => [
                'array' => [
                    [
                        'docFileName' => '37791.JPG',
                        'docId' => 3912734,
                        'docTypeId' => 5,
                        'docTypeName' => 'paveikslėlis',
                    ],
                ],
            ],
            'articleInfo' => '',
            'articleThumbnails' => [
                'array' => [
                    [
                        'thumbDocId' => 3912734,
                        'thumbFileName' => '0206\\37791.JPG',
                        'thumbTypeId' => 5,
                    ],
                    [
                        'thumbDocId' => 3912735,
                        'thumbFileName' => '0206\\37792.JPG',
                        'thumbTypeId' => 4,
                    ],
                ],
            ],
            'directArticle' => [
                'articleId' => 3211264,
                'articleName' => 'stabdžių trinkelių rinkinys, diskinis stabdys',
                'articleNo' => 37791,
                'articleState' => 1,
                'articleStateName' => 'įprastas',
                'brandName' => 'A.B.S.',
                'brandNo' => 206,
                'flagAccessories' => false,
                'flagCertificationCompulsory' => false,
                'flagRemanufacturedPart' => false,
                'flagSuitedforSelfService' => false,
                'genericArticleId' => 402,
                'hasAppendage' => false,
                'hasAxleLink' => false,
                'hasCsGraphics' => false,
                'hasDocuments' => true,
                'hasLessDiscount' => false,
                'hasMarkLink' => false,
                'hasMotorLink' => false,
                'hasOEN' => true,
                'hasPartList' => false,
                'hasPrices' => false,
                'hasSecurityInfo' => false,
                'hasUsage' => true,
                'hasVehicleLink' => true,
                'packingUnit' => 1,
                'quantityPerPackingUnit' => 4,
            ],
            'eanNumber' => [
                'array' => [
                    [
                        'eanNumber' => 8717109509571,
                    ],
                ],
            ],
            'oenNumbers' => [
                'array' => [
                    [
                        'blockNumber' => 1,
                        'brandName' => 'INFINITI',
                        'oeNumber' => '41060CA090',
                        'sortNumber' => 1,
                    ],
                    [
                        'blockNumber' => 2,
                        'brandName' => 'INFINITI',
                        'oeNumber' => '41060CA092',
                        'sortNumber' => 1,
                    ],
                ],
            ],
            'replacedByNumber' => '',
        ];

        return \json_decode(\json_encode($item, \JSON_THROW_ON_ERROR, 512), false, 512, \JSON_THROW_ON_ERROR);
    }
}
