<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Document;

class DocumentTest extends TestCase
{
    /**
     * @test
     */
    public function testDocumentCreationFromTecDocItem()
    {
        $tecDocDocumentItem = $this->getDocumentTecDocItem();

        $document = Document::createFromTecDocItem($tecDocDocumentItem);

        $this->assertInstanceOf(Document::class, $document);
        $this->assertEquals(1, $document->getId());
        $this->assertEquals(4, $document->getType());
        $this->assertEquals('Diegimo vadovas', $document->getName());
        $this->assertEquals('EINBAUEMPFEHLUNG_HEPU.PDF', $document->getFileName());
    }

    /**
     * @return \stdClass
     */
    protected function getDocumentTecDocItem()
    {
        $document = new \stdClass();
        $document->docId = 1;
        $document->docTypeId = 4;
        $document->docTypeName = 'Diegimo vadovas';
        $document->docFileName = 'EINBAUEMPFEHLUNG_HEPU.PDF';

        return $document;
    }
}
