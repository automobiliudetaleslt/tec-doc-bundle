<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use Nfq\Bundle\TecDocBundle\Entity\Filter;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\FilterValue;

class FilterTest extends TestCase
{
    /**
     * @test
     */
    public function testFilterCreationFromTecDocItem()
    {
        $filter = Filter::createFromTecDocItem($this->getTecDocItem());

        $this->assertInstanceOf(Filter::class, $filter);

        $this->assertEquals('važiuoklės numeris', $filter->getName());
        $this->assertEquals(26, $filter->getId());

        $this->assertCount(0, $filter->getValues());

        $filterValue = new FilterValue();

        $filter->addValue($filterValue);

        $this->assertCount(1, $filter->getValues());

        $this->assertEquals($filterValue, $filter->getValues()[0]);
    }

    /**
     * @return mixed
     */
    protected function getTecDocItem()
    {
        $item = [
            'attributeId' => 26,
            'attributeName' => 'važiuoklės numeris',
            'attributeShortName' => 'Važiuokl. Nr.',
            'attributeType' => 'A',
            'dutyFlag' => false,
            'interval' => false,
        ];

        return \json_decode(\json_encode($item, \JSON_THROW_ON_ERROR, 512), false, 512, JSON_THROW_ON_ERROR);
    }
}
