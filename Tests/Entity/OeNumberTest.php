<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\OeNumber;

class OeNumberTest extends TestCase
{
    /**
     * @test
     */
    public function testOeNumberCreationFromTecDocItem()
    {
        $tecDocOeNumberItem = $this->getOeNumberTecDocItem();

        $oeNumber = OeNumber::createFromTecDocItem($tecDocOeNumberItem);

        $this->assertTrue($oeNumber instanceof OeNumber);
        $this->assertEquals(123, $oeNumber->getNumber());
        $this->assertEquals('brand', $oeNumber->getBrand());
    }

    /**
     * @return \stdClass
     */
    protected function getOeNumberTecDocItem()
    {
        $oeNumber = new \stdClass();
        $oeNumber->oeNumber = 123;
        $oeNumber->brandName = 'brand';

        return $oeNumber;
    }
}
