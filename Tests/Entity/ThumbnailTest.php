<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Thumbnail;

class ThumbnailTest extends TestCase
{
    /**
     * @test
     */
    public function testThumbnailCreation()
    {
        $thumbNail = new Thumbnail(1, '2', 3);

        $this->assertEquals(1, $thumbNail->getId());
        $this->assertEquals('2', $thumbNail->getFileName());
        $this->assertEquals(3, $thumbNail->getTypeId());
    }

    /**
     * @test
     */
    public function testThumbnailCreationFromTecDocItem()
    {
        $tecDodItem = $this->getThumbnailTecDocItem();

        $thumbNail = Thumbnail::createFromTecDocItem($tecDodItem);

        $this->assertEquals(3912734, $thumbNail->getId());
        $this->assertEquals('0206\\37791.JPG', $thumbNail->getFileName());
        $this->assertEquals(5, $thumbNail->getTypeId());
    }

    /**
     * @return \stdClass
     */
    protected function getThumbnailTecDocItem(): \stdClass
    {
        $tecDocItem = [
            'thumbDocId' => 3912734,
            'thumbFileName' => '0206\\37791.JPG',
            'thumbTypeId' => 5,
        ];

        return \json_decode(\json_encode($tecDocItem, \JSON_THROW_ON_ERROR, 512), false, 512, \JSON_THROW_ON_ERROR);
    }
}
