<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use Nfq\Bundle\TecDocBundle\Exception\InvalidFilterValueTypeException;
use Nfq\Bundle\TecDocBundle\Entity\FilterValue;
use PHPUnit\Framework\TestCase;

class FilterValueTest extends TestCase
{
    /**
     * @test
     */
    public function testValueTypes()
    {
        $this->assertEquals('N', FilterValue::TYPE_NUMERIC);
        $this->assertEquals('A', FilterValue::TYPE_ALPHANUMERIC);
        $this->assertEquals('K', FilterValue::TYPE_KEY);
    }

    /**
     * @dataProvider valueDataProvider
     * @param array $values
     * @param mixed $expectedValue
     * @param mixed $expectedValueText
     * @param mixed $expectedType
     * @throws InvalidFilterValueTypeException
     */
    public function testNumericValueCreation(array $values, $expectedValue, $expectedValueText, $expectedType)
    {
        list($selected, $notSelected) = $values;

        $selected    = FilterValue::createFromTecDocItem($selected);
        $notSelected = FilterValue::createFromTecDocItem($notSelected);

        /** @var FilterValue[] $filterValues */
        $filterValues = [$selected, $notSelected];

        foreach ($filterValues as $filterValue) {
            $this->assertTrue($filterValue instanceof FilterValue);
            $this->assertEquals($expectedType, $filterValue->getType());
            $this->assertEquals($expectedValue, $filterValue->getValue());

            $this->assertEquals($expectedValueText, $filterValue->getValueText());
            $this->assertEquals(560, $filterValue->getId());
        }

        $this->assertTrue($selected->getSelected());
        $this->assertFalse($notSelected->getSelected());
    }

    /**
     * @return array
     */
    public function valueDataProvider()
    {
        $noValueSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => true,
            'selected' => true,
            'type' => 'N',
        ];

        $noValueNotSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => true,
            'selected' => false,
            'type' => 'N',
        ];

        $numericSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'numValue' => 100,
            'selected' => true,
            'type' => 'N',
        ];

        $numericNotSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'numValue' => 100,
            'selected' => false,
            'type' => 'N',
        ];

        $alphanumericSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'alphaValue' => 200,
            'selected' => true,
            'type' => 'A',
        ];

        $alphanumericNotSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'alphaValue' => 200,
            'selected' => false,
            'type' => 'A',
        ];

        $keySelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'keyValue' => 'value',
            'keyValueText' => 'text',
            'selected' => true,
            'type' => 'K',
        ];

        $keyNotSelected = [
            'attributeId' => 560,
            'count' => 82,
            'noValue' => false,
            'keyValue' => 'value',
            'keyValueText' => 'text',
            'selected' => false,
            'type' => 'K',
        ];

        return [
            [
                [
                    $this->createStdClassFromArray($noValueSelected),
                    $this->createStdClassFromArray($noValueNotSelected),
                ],
                null,
                null,
                'N',
            ],
            [
                [
                    $this->createStdClassFromArray($numericSelected),
                    $this->createStdClassFromArray($numericNotSelected),
                ],
                100,
                100,
                'N',
            ],
            [
                [
                    $this->createStdClassFromArray($alphanumericSelected),
                    $this->createStdClassFromArray($alphanumericNotSelected),
                ],
                200,
                200,
                'A',
            ],
            [
                [
                    $this->createStdClassFromArray($keySelected),
                    $this->createStdClassFromArray($keyNotSelected),
                ],
                'value',
                'text',
                'K',
            ],
        ];
    }

    /**
     * @dataProvider filterValueToTecDocItemDataProvider
     * @param FilterValue $filterValue
     * @param array $expectedTecDocItem
     */
    public function testTecDocItemCreation(FilterValue $filterValue, array $expectedTecDocItem)
    {
        $this->assertEquals($expectedTecDocItem, $filterValue->createTecDocItemStructure());
    }

    /**
     * @return array
     * @throws InvalidFilterValueTypeException
     */
    public function filterValueToTecDocItemDataProvider()
    {
        $noValueFilterSelected = new FilterValue();
        $noValueFilterSelected->setId(1)->setSelected(true)->setNoValue(true)->setType(FilterValue::TYPE_NUMERIC);
        $noValueFilterNotSelected = new FilterValue();
        $noValueFilterNotSelected->setId(1)->setSelected(false)->setNoValue(true)->setType(FilterValue::TYPE_NUMERIC);

        $numericFilterSelected = new FilterValue();
        $numericFilterSelected
            ->setId(1)
            ->setSelected(true)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_NUMERIC)
            ->setValue('100')
            ->setValueText('100');
        $numericFilterNotSelected = new FilterValue();
        $numericFilterNotSelected
            ->setId(1)
            ->setSelected(false)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_NUMERIC)
            ->setValue('100')
            ->setValueText('100');

        $alphnumericFilterSelected = new FilterValue();
        $alphnumericFilterSelected
            ->setId(1)
            ->setSelected(true)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_ALPHANUMERIC)
            ->setValue('100')
            ->setValueText('100');
        $alphnumericFilterNotSelected = new FilterValue();
        $alphnumericFilterNotSelected
            ->setId(1)
            ->setSelected(false)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_ALPHANUMERIC)
            ->setValue('100')
            ->setValueText('100');

        $keyFilterSelected = new FilterValue();
        $keyFilterSelected
            ->setId(1)
            ->setSelected(true)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_KEY)
            ->setValue('100')
            ->setValueText('text');
        $keyFilterNotSelected = new FilterValue();
        $keyFilterNotSelected
            ->setId(1)
            ->setSelected(false)
            ->setNoValue(false)
            ->setType(FilterValue::TYPE_KEY)
            ->setValue('100')
            ->setValueText('text');

        return [
            [
                $noValueFilterSelected,
                [
                    'attributeId' => 1,
                    'noValue' => true,
                    'selected' => true,
                    'type' => 'N',
                ],
            ],
            [
                $noValueFilterNotSelected,
                [
                    'attributeId' => 1,
                    'noValue' => true,
                    'selected' => false,
                    'type' => 'N',
                ],
            ],
            [
                $numericFilterSelected,
                [
                    'attributeId' => 1,
                    'numValue' => 100,
                    'selected' => true,
                    'type' => 'N',
                    'noValue' => false,
                ],
            ],
            [
                $numericFilterNotSelected,
                [
                    'attributeId' => 1,
                    'numValue' => 100,
                    'selected' => false,
                    'type' => 'N',
                    'noValue' => false,
                ],
            ],
            [
                $alphnumericFilterSelected,
                [
                    'attributeId' => 1,
                    'alphaValue' => 100,
                    'selected' => true,
                    'type' => 'A',
                    'noValue' => false,
                ],
            ],
            [
                $alphnumericFilterNotSelected,
                [
                    'attributeId' => 1,
                    'alphaValue' => 100,
                    'selected' => false,
                    'type' => 'A',
                    'noValue' => false,
                ],
            ],
            [
                $keyFilterSelected,
                [
                    'attributeId' => 1,
                    'keyValue' => 100,
                    'keyValueText' => 'text',
                    'selected' => true,
                    'type' => 'K',
                    'noValue' => false,
                ],
            ],
            [
                $keyFilterNotSelected,
                [
                    'attributeId' => 1,
                    'keyValue' => 100,
                    'keyValueText' => 'text',
                    'selected' => false,
                    'type' => 'K',
                    'noValue' => false,
                ],
            ],
        ];
    }

    /**
     * @param array $data
     * @return \stdClass
     */
    protected function createStdClassFromArray(array $data): \stdClass
    {
        return \json_decode(\json_encode($data, \JSON_THROW_ON_ERROR, 512), false, 512, \JSON_THROW_ON_ERROR);
    }
}
