<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;

class VehicleManufacturerTest extends TestCase
{
    /**
     * @test
     */
    public function testVehicleManufacturerCreation()
    {
        $data = [
            'manuId' => 1526,
            'manuName' => 'INFINITI',
        ];

        $tecDocManuData = \json_decode(
            \json_encode($data, \JSON_THROW_ON_ERROR, 512),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $manufacturer = VehicleManufacturer::createFromTecDocItem($tecDocManuData);

        $this->assertEquals($data['manuId'], $manufacturer->getId());
        $this->assertEquals($data['manuName'], $manufacturer->getTitle());
    }
}
