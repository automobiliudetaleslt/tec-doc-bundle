<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Cache;

use Nfq\Bundle\TecDocBundle\Provider\LocaleProvider;
use Nfq\Bundle\TecDocBundle\Transformer\IterableTransformer;

class CacheKeyGenerator
{
    /**
     * @var IterableTransformer
     */
    private $transformer;

    /**
     * @var LocaleProvider
     */
    private $localeProvider;

    /**
     * @param IterableTransformer $transformer
     * @param LocaleProvider $localeProvider
     */
    public function __construct(IterableTransformer $transformer, LocaleProvider $localeProvider)
    {
        $this->transformer = $transformer;
        $this->localeProvider = $localeProvider;
    }

    /**
     * @param iterable $values
     * @return string
     */
    public function generate(iterable $values): string
    {
        return $this->transformer->transformToHash([$values, $this->localeProvider->getLocale()]);
    }
}
