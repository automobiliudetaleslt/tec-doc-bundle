<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Loader;

use Symfony\Component\Yaml\Yaml;

class YamlMountingPlaceFileLoader
{
    /**
     * @var string
     */
    protected $pathToYaml;


    /**
     * @param string $pathToYaml
     */
    public function __construct(string $pathToYaml)
    {
        $this->pathToYaml = \sprintf('%s/../Resources/config/%s', __DIR__, $pathToYaml);
    }

    /**
     * @return mixed
     */
    public function getFileData()
    {
        return Yaml::parse((string)\file_get_contents($this->pathToYaml));
    }
}
