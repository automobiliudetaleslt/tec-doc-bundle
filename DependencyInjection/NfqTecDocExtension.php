<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class NfqTecDocExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $config[Configuration::METHOD_TIMEOUT] = $this->prepareMethodTimeoutConfig(
            $config[Configuration::METHOD_TIMEOUT]
        );

        $this->addParameter($config, $container, Configuration::TIMEOUT);
        $this->addParameter($config, $container, Configuration::CONNECT_TIMEOUT);
        $this->addParameter($config, $container, Configuration::METHOD_TIMEOUT);
        $this->addParameter($config, $container, Configuration::ARTICLE_COUNTRY);
        $this->addParameter($config, $container, Configuration::LANG);
        $this->addParameter($config, $container, Configuration::PROVIDER);
        $this->addParameter($config, $container, Configuration::API_KEY);
        $this->addParameter($config, $container, Configuration::API_ENDPOINT_URI);
        $this->addParameter($config, $container, Configuration::MEDIA_URI);
        $this->addParameter($config, $container, Configuration::CATEGORY_SLUG_SEPARATOR);
        $this->addParameter($config, $container, Configuration::USED_TECDOC_FILTERS_IDS);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->processCacheStorage($config, $container);
        $this->processRootCategoryNameProvider($config, $container);
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     * @param string $parameterKey
     */
    private function addParameter(array $config, ContainerBuilder $container, string $parameterKey): void
    {
        $container->setParameter(\sprintf('%s.%s', $this->getAlias(), $parameterKey), $config[$parameterKey]);
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function processCacheStorage(array $config, ContainerBuilder $container): void
    {
        $cachedApiManager = $container->findDefinition('nfq_tec_doc.cache.manager');
        $cachedApiManager->replaceArgument(0, new Reference($config[Configuration::CACHE_STORAGE]));
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function processRootCategoryNameProvider(array $config, ContainerBuilder $container): void
    {
        $rootCategoryGenerator = $container->findDefinition('nfq_tec_doc.generator.root_category');
        $rootCategoryGenerator->replaceArgument(1, new Reference($config[Configuration::ROOT_CATEGORY_NAME_PROVIDER]));
    }

    /**
     * @param array $config
     * @return array
     */
    private function prepareMethodTimeoutConfig(array $config): array
    {
        // Add default values
        $config[] = [
            Configuration::METHOD_TIMEOUT_METHOD => 'getArticleDirectSearchAllNumbersWithState',
            Configuration::METHOD_TIMEOUT_TIMEOUT => 15,
        ];
        $config[] = [
            Configuration::METHOD_TIMEOUT_METHOD => 'getArticleAccessoryList4',
            Configuration::METHOD_TIMEOUT_TIMEOUT => 15,
        ];

        // Sort by timeout
        \usort(
            $config,
            static function (array $arr1, array $arr2): int {
                return $arr2[Configuration::METHOD_TIMEOUT_TIMEOUT] <=> $arr1[Configuration::METHOD_TIMEOUT_TIMEOUT];
            }
        );

        // Remove duplicate method values
        $config = \array_intersect_key(
            $config,
            \array_unique(\array_column($config, Configuration::METHOD_TIMEOUT_METHOD))
        );

        return \array_values($config);
    }
}
