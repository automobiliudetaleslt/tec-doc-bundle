<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public const TIMEOUT = 'timeout';
    public const CONNECT_TIMEOUT = 'connect_timeout';
    public const METHOD_TIMEOUT = 'method_timeout';
    public const METHOD_TIMEOUT_METHOD = 'method';
    public const METHOD_TIMEOUT_TIMEOUT = 'timeout';
    public const ARTICLE_COUNTRY = 'article_country';
    public const LANG = 'lang';
    public const PROVIDER = 'provider';
    public const API_KEY = 'api_key';
    public const API_ENDPOINT_URI = 'api_endpoint_uri';
    public const MEDIA_URI = 'media_uri';
    public const CACHE_STORAGE = 'cache_storage';
    public const CATEGORY_SLUG_SEPARATOR = 'category_slug_separator';
    public const ROOT_CATEGORY_NAME_PROVIDER = 'root_category_name_provider';
    public const USED_TECDOC_FILTERS_IDS = 'used_tecdoc_filters_ids';

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('nfq_tec_doc');
        // Keep compatibility with sf < 4.2
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('nfq_tec_doc');
        }

        $rootNode
            ->children()
                ->integerNode(self::TIMEOUT)
                    ->defaultValue(3)
                ->end()
                ->integerNode(self::CONNECT_TIMEOUT)
                    ->defaultValue(3)
                ->end()
                ->arrayNode(self::METHOD_TIMEOUT)
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode(self::METHOD_TIMEOUT_METHOD)->end()
                            ->integerNode(self::METHOD_TIMEOUT_TIMEOUT)->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode(self::ARTICLE_COUNTRY)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::LANG)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::PROVIDER)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::API_KEY)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::API_ENDPOINT_URI)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::MEDIA_URI)
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode(self::CACHE_STORAGE)
                    ->defaultValue('nfq_tec_doc.cache.storage.null')
                ->end()
                ->scalarNode(self::CATEGORY_SLUG_SEPARATOR)
                    ->defaultValue('/')
                ->end()
                ->scalarNode(self::ROOT_CATEGORY_NAME_PROVIDER)
                    ->defaultValue('nfq_tec_doc.provider.default_root_category_name')
                ->end()
                ->arrayNode(self::USED_TECDOC_FILTERS_IDS)
                    ->prototype('scalar')->end()
                    ->defaultValue([])
                ->end()
            ->end();

        return $treeBuilder;
    }
}
