<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Entity\Collections\ArrayCollection;
use Nfq\Bundle\TecDocBundle\Exception\FieldNotFoundException;
use Nfq\Bundle\TecDocBundle\Helpers\Str;

class Article
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $linkId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $brandName;

    /**
     * @var string
     */
    protected $brandNumber;

    /**
     * @var string
     */
    protected $genericArticleId;

    /**
     * @var ArrayCollection|OeNumber[]
     */
    protected $oeNumbers;

    /**
     * @var ArrayCollection
     */
    protected $documents;

    /**
     * @var ArrayCollection
     */
    protected $thumbnails;

    /**
     * @var ArrayCollection
     */
    protected $attributes;

    /**
     * @var ArrayCollection|ArticleInfo[]
     */
    protected $articleInfo;

    /**
     * @var bool
     */
    protected $hasEquipment;

    /**
     * @var bool
     */
    protected $hasAccessories;

    /**
     * @var bool
     */
    protected $hasVehicles;

    /**
     * @var int
     */
    protected $packingUnit = 1;

    /**
     * @var ArrayCollection
     */
    protected $eanNumbers;

    /**
     * @var string|null
     */
    protected $replacedId;

    /**
     * @var ArticleState
     */
    protected $state;

    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->hasEquipment = false;
        $this->hasAccessories = false;
        $this->hasVehicles = false;
        $this->oeNumbers = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->thumbnails = new ArrayCollection();
        $this->attributes = new ArrayCollection();
        $this->articleInfo = new ArrayCollection();
        $this->eanNumbers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Article
     */
    public function setId(int $id): Article
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getLinkId(): int
    {
        return $this->linkId;
    }

    /**
     * @param int $linkId
     * @return Article
     */
    public function setLinkId(int $linkId): Article
    {
        $this->linkId = $linkId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Article
     */
    public function setName(string $name): Article
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Article
     */
    public function setNumber(string $number): Article
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    /**
     * @param string $brandName
     * @return Article
     */
    public function setBrandName(string $brandName): Article
    {
        $this->brandName = $brandName;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrandNumber(): ?string
    {
        return $this->brandNumber;
    }

    /**
     * @param string $brandNumber
     * @return Article
     */
    public function setBrandNumber(string $brandNumber): Article
    {
        $this->brandNumber = $brandNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenericArticleId(): ?string
    {
        return $this->genericArticleId;
    }

    /**
     * @param string $genericArticleId
     * @return Article
     */
    public function setGenericArticleId(string $genericArticleId): Article
    {
        $this->genericArticleId = $genericArticleId;

        return $this;
    }

    /**
     * @param OeNumber $oeNumber
     * @return Article
     */
    public function addOeNumber(OeNumber $oeNumber): Article
    {
        $this->oeNumbers->add($oeNumber);

        return $this;
    }

    /**
     * @return ArrayCollection|OeNumber[]
     */
    public function getOeNumbers(): ArrayCollection
    {
        return $this->oeNumbers;
    }

    /**
     * @return bool
     */
    public function hasOeNumbers(): bool
    {
        return !$this->oeNumbers->isEmpty();
    }

    /**
     * @param Document $document
     * @return Article
     */
    public function addDocument(Document $document): Article
    {
        $this->documents->add($document);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments(): ArrayCollection
    {
        return $this->documents;
    }

    /**
     * @return bool
     */
    public function hasDocuments(): bool
    {
        return !$this->documents->isEmpty();
    }

    /**
     * @param Thumbnail $thumbnail
     * @return Article
     */
    public function addThumbnail(Thumbnail $thumbnail): Article
    {
        $this->thumbnails->add($thumbnail);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getThumbnails(): ArrayCollection
    {
        return $this->thumbnails;
    }

    /**
     * @return bool
     */
    public function hasThumbnails(): bool
    {
        return !$this->thumbnails->isEmpty();
    }

    /**
     * @param Attribute $attribute
     * @return Article
     */
    public function addAttribute(Attribute $attribute): Article
    {
        $this->attributes->add($attribute);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttributes(): ArrayCollection
    {
        return $this->attributes;
    }

    /**
     * @param ArticleInfo $articleInfo
     * @return Article
     */
    public function addArticleInfo(ArticleInfo $articleInfo): Article
    {
        $this->articleInfo->add($articleInfo);

        return $this;
    }

    /**
     * @return ArrayCollection|ArticleInfo[]
     */
    public function getArticleInfo(): ArrayCollection
    {
        return $this->articleInfo;
    }

    /**
     * @param bool $hasEquipment
     * @return Article
     */
    public function setHasEquipment(bool $hasEquipment): Article
    {
        $this->hasEquipment = $hasEquipment;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasEquipment(): bool
    {
        return $this->hasEquipment;
    }

    /**
     * @param bool $hasAccessories
     * @return Article
     */
    public function setHasAccessories(bool $hasAccessories): Article
    {
        $this->hasAccessories = $hasAccessories;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAccessories(): bool
    {
        return $this->hasAccessories;
    }

    /**
     * @param bool $hasVehicles
     * @return Article
     */
    public function setHasVehicles(bool $hasVehicles): Article
    {
        $this->hasVehicles = $hasVehicles;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasVehicles(): bool
    {
        return $this->hasVehicles;
    }

    /**
     * @return int
     */
    public function getPackingUnit(): int
    {
        return $this->packingUnit;
    }

    /**
     * @param int $packingUnit
     * @return Article
     */
    public function setPackingUnit(int $packingUnit): Article
    {
        $this->packingUnit = $packingUnit;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEanNumbers(): ArrayCollection
    {
        return $this->eanNumbers;
    }

    /**
     * @param ArrayCollection $eanNumbers
     * @return Article
     */
    public function setEanNumbers(ArrayCollection $eanNumbers): Article
    {
        $this->eanNumbers = $eanNumbers;

        return $this;
    }

    /**
     * @param string $eanNumber
     * @return Article
     */
    public function addEanNumber(string $eanNumber): Article
    {
        $this->eanNumbers->add($eanNumber);
        
        return $this;
    }

    /**
     * @return bool
     */
    public function hasEanNumbers(): bool
    {
        return !$this->eanNumbers->isEmpty();
    }

    /**
     * @return string|null
     */
    public function getReplacedId(): ?string
    {
        return $this->replacedId;
    }

    /**
     * @param string|null $replacedId
     * @return Article
     */
    public function setReplacedId(string $replacedId = null): Article
    {
        $this->replacedId = $replacedId;

        return $this;
    }

    /**
     * @return ArticleState
     */
    public function getState(): ArticleState
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return null !== $this->state;
    }

    /**
     * @param ArticleState $state
     * @return Article
     */
    public function setState(ArticleState $state): Article
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return null|Weight
     */
    public function getWeight(): ?Weight
    {
        /** @var Attribute $attribute */
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->isWeightKilogramsAttribute()) {
                return new Weight(
                    (float)\str_replace(',', '.', $attribute->getValue()),
                    Weight::UNIT_KILOGRAM
                );
            }
            if ($attribute->isWeightGramsAttribute()) {
                return new Weight(
                    (float)\str_replace(',', '.', $attribute->getValue()),
                    Weight::UNIT_GRAM
                );
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPairedNumber(): string
    {
        /** @var Attribute $attribute */
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->isPairedNumberAttribute()) {
                return $attribute->getValue();
            }
        }

        return '';
    }

    /**
     * @param \stdClass $item
     * @return Article
     * @throws FieldNotFoundException
     */
    public static function createFromTecDocItem(\stdClass $item): Article
    {
        if (!isset($item->directArticle)) {
            throw new FieldNotFoundException('directArticle field not found.');
        }

        $article = new self();

        $tecDocArticle = $item->directArticle;
        $thumbnails = $item->articleThumbnails;
        $attributes = $item->articleAttributes;
        $articleInfo = $item->articleInfo;
        $documents = $item->articleDocuments;
        $oeNumbers = $item->oenNumbers;
        $eanNumbers = $item->eanNumber;

        $article
            ->setId($tecDocArticle->articleId)
            ->setName(Str::ucfirst($tecDocArticle->articleName))
            ->setNumber($tecDocArticle->articleNo)
            ->setBrandName($tecDocArticle->brandName ?? '')
            ->setBrandNumber($tecDocArticle->brandNo ?? '')
            ->setHasEquipment($tecDocArticle->hasPartList)
            ->setHasAccessories($tecDocArticle->hasAppendage)
            ->setHasVehicles($tecDocArticle->hasVehicleLink)
            ->setGenericArticleId($tecDocArticle->genericArticleId);

        if (isset($tecDocArticle->articleState, $tecDocArticle->articleStateName)) {
            $article->setState(new ArticleState($tecDocArticle->articleState, $tecDocArticle->articleStateName));
        }

        if (isset($tecDocArticle->packingUnit)) {
            $article->setPackingUnit($tecDocArticle->packingUnit);
        }

        if (isset($thumbnails->array)) {
            foreach ($thumbnails->array as $tecDocThumbnail) {
                $article->addThumbnail(Thumbnail::createFromTecDocItem($tecDocThumbnail));
            }
        }

        if (isset($attributes->array)) {
            foreach ($attributes->array as $tecDocAttribute) {
                $article->addAttribute(Attribute::createFromTecDocItem($tecDocAttribute));
            }
        };

        if (isset($articleInfo->array)) {
            foreach ($articleInfo->array as $info) {
                $article->addArticleInfo(ArticleInfo::createFromTecDocItem($info));
            }
        }

        if (isset($documents->array)) {
            foreach ($documents->array as $tecDocDocument) {
                $article->addDocument(Document::createFromTecDocItem($tecDocDocument));
            }
        };

        if (isset($oeNumbers->array)) {
            foreach ($oeNumbers->array as $tecDocOeNumber) {
                $article->addOeNumber(OeNumber::createFromTecDocItem($tecDocOeNumber));
            }
        };
        
        if (isset($eanNumbers->array)) {
            foreach ($eanNumbers->array as $tecDocEanNumber) {
                $article->addEanNumber($tecDocEanNumber->eanNumber);
            }
        }

        if (isset($item->replacedByNumber, $item->replacedByNumber->array) && !empty($item->replacedByNumber->array)) {
            $replacedItem = \reset($item->replacedByNumber->array);
            if ($replacedItem && isset($replacedItem->replaceArticleId)) {
                $article->setReplacedId($replacedItem->replaceArticleId);
            }
        }

        return $article;
    }

    /**
     * @param \stdClass $item
     * @return Article
     */
    public static function createFromSimpleTecDocItem(\stdClass $item): Article
    {
        $article = new self();
        $article
            ->setId($item->articleId)
            ->setNumber($item->articleNo)
            ->setBrandName($item->brandName)
            ->setBrandNumber($item->brandNo)
            ->setGenericArticleId($item->genericArticleId);

        if (isset($item->articleLinkId)) {
            $article->setLinkId($item->articleLinkId);
        }
        
        return $article;
    }
}
