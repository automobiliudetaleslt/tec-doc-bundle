<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Exception\InvalidFilterValueTypeException;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;

class FilterValue
{
    const TYPE_NUMERIC = 'N';
    const TYPE_ALPHANUMERIC = 'A';
    const TYPE_KEY = 'K';

    const SUPPORTED_TYPES = [
        self::TYPE_NUMERIC,
        self::TYPE_ALPHANUMERIC,
        self::TYPE_KEY,
    ];

    /**
     * @var int
     */
    protected $id;

    /**
     * @var bool
     */
    protected $selected;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $valueText = '';

    /**
     * @var bool
     */
    protected $noValue;

    /**
     * @var string
     */
    protected $type;

    /**
     * FilterValue constructor.
     */
    public function __construct()
    {
        $this->noValue = true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FilterValue
     */
    public function setId(int $id): FilterValue
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSelected(): bool
    {
        return $this->selected;
    }

    /**
     * @param bool $selected
     * @return FilterValue
     */
    public function setSelected(bool $selected): FilterValue
    {
        $this->selected = $selected;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return FilterValue
     */
    public function setValue(string $value): FilterValue
    {
        $this->value = $value;
        $this->setNoValue('' == $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getValueText(): string
    {
        return $this->valueText;
    }

    /**
     * @param string $valueText
     * @return FilterValue;
     */
    public function setValueText(string $valueText): FilterValue
    {
        $this->valueText = $valueText;

        return $this;
    }

    /**
     * @param bool $noValue
     * @return FilterValue
     */
    public function setNoValue(bool $noValue): FilterValue
    {
        $this->noValue = $noValue;

        return $this;
    }

    /**
     * @return bool
     */
    public function getNoValue(): bool
    {
        return $this->noValue;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FilterValue
     * @throws InvalidFilterValueTypeException
     */
    public function setType(string $type): FilterValue
    {
        if (!\in_array($type, self::SUPPORTED_TYPES, true)) {
            throw new InvalidFilterValueTypeException(\sprintf('Type "%s" is not a valid %s type.', $type, __CLASS__));
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function createTecDocItemStructure(): array
    {
        $item = [
            'attributeId' => $this->id,
            'selected' => $this->selected,
            'type' => $this->type,
            'noValue' => $this->noValue,
        ];

        if ($this->noValue) {
            return $item;
        }

        $valueTypeMap = [
            self::TYPE_NUMERIC => 'numValue',
            self::TYPE_ALPHANUMERIC => 'alphaValue',
            self::TYPE_KEY => 'keyValue',
        ];

        if (!Arr::keyExists($this->type, $valueTypeMap)) {
            throw new \LogicException('You must set value type.');
        }

        $item[$valueTypeMap[$this->type]] = $this->value;

        if (self::TYPE_KEY == $this->type) {
            $item['keyValueText'] = $this->getValueText();
        }

        return $item;
    }

    /**
     * @param \stdClass $item
     * @return FilterValue
     * @throws InvalidFilterValueTypeException
     */
    public static function createFromTecDocItem(\stdClass $item): FilterValue
    {
        $filterValue = new self();

        $filterValue->setSelected($item->selected);
        $filterValue->setId($item->attributeId);

        $type = $item->type;
        $filterValue->setType($type);

        if ($item->noValue) {
            $filterValue->setNoValue(true);

            return $filterValue;
        }
        $filterValue->setNoValue(false);

        $valueGetter = null;
        $valueTextGetter = null;

        if ($type == self::TYPE_NUMERIC) {
            $valueGetter = 'numValue';
            $valueTextGetter = 'numValue';
        } elseif ($type == self::TYPE_ALPHANUMERIC) {
            $valueGetter = 'alphaValue';
            $valueTextGetter = 'alphaValue';
        } else {
            $valueGetter = 'keyValue';
            $valueTextGetter = 'keyValueText';
        }

        $filterValue->setValue((string)$item->$valueGetter);
        $filterValue->setValueText((string)$item->$valueTextGetter);

        return $filterValue;
    }
}
