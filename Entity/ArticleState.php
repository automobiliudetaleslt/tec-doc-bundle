<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class ArticleState
{
    private const STATE_NOT_AVAILABLE = 2;
    private const STATE_ONLY_SUPPLIED_IN_PARTS_LIST = 5;
    private const STATE_NO_LONGER_SUPPLIED_BY_MANUFACTURER = 9;
    private const STATE_NOT_SUPPLIED_INDIVIDUALLY = 12;

    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $name;

    /**
     * @param int $number
     * @param string $name
     */
    public function __construct(int $number, string $name)
    {
        $this->number = $number;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isNotAvailable(): bool
    {
        return self::STATE_NOT_AVAILABLE === $this->getNumber();
    }

    /**
     * @return bool
     */
    public function isOnlySuppliedInPartsList(): bool
    {
        return self::STATE_ONLY_SUPPLIED_IN_PARTS_LIST === $this->getNumber();
    }

    /**
     * @return bool
     */
    public function isNoLongerSuppliedByManufacturer(): bool
    {
        return self::STATE_NO_LONGER_SUPPLIED_BY_MANUFACTURER === $this->getNumber();
    }

    /**
     * @return bool
     */
    public function isNotSuppliedIndividually(): bool
    {
        return self::STATE_NOT_SUPPLIED_INDIVIDUALLY === $this->getNumber();
    }
}
