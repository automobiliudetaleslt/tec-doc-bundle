<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Helpers\DateParser;

class VehicleModel
{
    const DATA_SOURCE_VEHICLE = 'V';
    const DATA_SOURCE_MODEL = 'M';

    const DATE_FROM_FIELD_SOURCE_VEHICLE = 'yearOfConstructionFrom';
    const DATE_TO_FIELD_SOURCE_VEHICLE = 'yearOfConstructionTo';
    const DATE_FROM_FIELD_SOURCE_MODEL = 'yearOfConstrFrom';
    const DATE_TO_FIELD_SOURCE_MODEL = 'yearOfConstrTo';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var \DateTime
     */
    protected $yearOfConstructionFrom;

    /**
     * @var \DateTime
     */
    protected $yearOfConstructionTo;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return VehicleModel;
     */
    public function setId(int $id): VehicleModel
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VehicleModel;
     */
    public function setTitle(string $title): VehicleModel
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getYearOfConstructionFrom()
    {
        return $this->yearOfConstructionFrom;
    }

    /**
     * @param \DateTime $yearOfConstructionFrom
     * @return VehicleModel
     */
    public function setYearOfConstructionFrom(\DateTime $yearOfConstructionFrom): VehicleModel
    {
        $this->yearOfConstructionFrom = $yearOfConstructionFrom;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getYearOfConstructionTo()
    {
        return $this->yearOfConstructionTo;
    }

    /**
     * @param \DateTime $yearOfConstructionTo
     * @return VehicleModel
     */
    public function setYearOfConstructionTo(\DateTime $yearOfConstructionTo): VehicleModel
    {
        $this->yearOfConstructionTo = $yearOfConstructionTo;

        return $this;
    }

    /**
     * @return string
     */
    public function getYearOfConstructionInterval(): string
    {
        return \sprintf(
            '%s - %s',
            $this->getFormatedYearOfConstructionFrom(),
            $this->getFormatedYearOfConstructionTo()
        );
    }

    /**
     * @param string $format
     * @return string
     */
    public function getFormatedYearOfConstructionFrom(string $format = 'Y-m'): string
    {
        return $this->formatYearOfConstruction($this->getYearOfConstructionFrom(), $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function getFormatedYearOfConstructionTo(string $format = 'Y-m'): string
    {
        return $this->formatYearOfConstruction($this->getYearOfConstructionTo(), $format);
    }

    /**
     * @param \stdClass $item
     * @param string $dataSource
     * @return VehicleModel
     */
    public static function createFromTecDocItem(
        \stdClass $item,
        string $dataSource = self::DATA_SOURCE_MODEL
    ): VehicleModel {
        $vehicleModel = new self();
        $vehicleModel->setId($item->modelId);

        if ($dataSource == self::DATA_SOURCE_MODEL) {
            $vehicleModel->setTitle($item->modelname);
            $dateFromField = self::DATE_FROM_FIELD_SOURCE_MODEL;
            $dateToField = self::DATE_TO_FIELD_SOURCE_MODEL;
        } elseif ($dataSource == self::DATA_SOURCE_VEHICLE) {
            $vehicleModel->setTitle($item->modelDesc);
            $dateFromField = self::DATE_FROM_FIELD_SOURCE_VEHICLE;
            $dateToField = self::DATE_TO_FIELD_SOURCE_VEHICLE;
        } else {
            throw new \LogicException('Unsupported data source.');
        }

        if (isset($item->$dateFromField)) {
            $yearOfConstructionFrom = DateParser::getDateTime($item->$dateFromField);

            $vehicleModel->setYearOfConstructionFrom($yearOfConstructionFrom);

            if (isset($item->$dateToField)) {
                $yearOfConstructionTo = DateParser::getDateTime($item->$dateToField);
                $vehicleModel->setYearOfConstructionTo($yearOfConstructionTo);
            } else {
                $vehicleModel->setYearOfConstructionTo(new \DateTime('today'));
            }
        }

        return $vehicleModel;
    }

    /**
     * @param \DateTimeInterface|null $date
     * @param string $format
     * @return string
     */
    private function formatYearOfConstruction(?\DateTimeInterface $date, string $format = 'Y-m'): string
    {
        return null !== $date ? $date->format($format) : '';
    }
}
