<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;
use Nfq\Bundle\TecDocBundle\Entity\Category;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\Exception\NotFoundException;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Helpers\Str;
use Nfq\Bundle\TecDocBundle\Resolver\VehicleTargetTypeResolver;
use Nfq\Bundle\TecDocBundle\TargetType;

class VehicleManager
{
    private const CHUNK_SIZE = 25;

    public const TYPE_PASSENGER = 1;
    public const TYPE_COMMERCIAL = 2;
    public const TYPE_MOTORCYCLE = 3;
    public const TYPE_ALL = 4;

    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @var VehicleTargetTypeResolver
     */
    protected $vehicleTargetTypeResolver;

    /**
     * @param TecDocApiManagerInterface $apiManager
     * @param VehicleTargetTypeResolver $vehicleTargetTypeResolver
     */
    public function __construct(
        TecDocApiManagerInterface $apiManager,
        VehicleTargetTypeResolver $vehicleTargetTypeResolver
    ) {
        $this->apiManager = $apiManager;
        $this->vehicleTargetTypeResolver = $vehicleTargetTypeResolver;
    }

    /**
     * @param int $articleId
     * @return VehicleManufacturer[]
     */
    public function geArticleCompatibleVehicleManufacturers(int $articleId): array
    {
        $manufacturers = [];

        $tecDocManufacturers = $this->apiManager->getArticleLinkedAllLinkingTargetManufacturer2($articleId);

        foreach ($tecDocManufacturers as $tecDocManufacturer) {
            $manufacturer = VehicleManufacturer::createFromTecDocItem($tecDocManufacturer);
            $manufacturers[$manufacturer->getId()] = $manufacturer;
        }

        return $manufacturers;
    }

    /**
     * @param int $vehicleType
     * @return VehicleManufacturer[]
     */
    public function getVehicleManufacturers(int $vehicleType = self::TYPE_PASSENGER): array
    {
        $tecDocManufacturers = $this->apiManager->getManufacturers(
            $this->convertVehicleTypeFlag($vehicleType)
        );

        $manufacturers = [];

        foreach ($tecDocManufacturers as $tecDocManufacturer) {
            $manufacturerName = $tecDocManufacturer->manuName;
            $manufacturer = null;

            if (self::TYPE_ALL === $vehicleType) {
                $manufacturer = VehicleManufacturer::createFromTecDocItem($tecDocManufacturer);
            } else {
                if (self::TYPE_MOTORCYCLE === $vehicleType && $this->isMotorcycle($manufacturerName)) {
                    $manufacturer = VehicleManufacturer::createFromTecDocItem($tecDocManufacturer);
                }

                if (self::TYPE_MOTORCYCLE !== $vehicleType && !$this->isMotorcycle($manufacturerName)) {
                    $manufacturer = VehicleManufacturer::createFromTecDocItem($tecDocManufacturer);
                }
            }

            if (null !== $manufacturer) {
                $manufacturers[$manufacturer->getId()] = $manufacturer;
            }
        }

        return $manufacturers;
    }

    /**
     * @param int $manufacturerId
     * @param int $vehicleType
     * @return VehicleManufacturer
     * @throws NotFoundException
     */
    public function getVehicleManufacturerById(
        int $manufacturerId,
        int $vehicleType = self::TYPE_PASSENGER
    ): VehicleManufacturer {
        $manufacturers = $this->getVehicleManufacturers($vehicleType);

        if (isset($manufacturers[$manufacturerId])) {
            return $manufacturers[$manufacturerId];
        }

        throw new NotFoundException(\sprintf('Manufacturer %d does not exist', $manufacturerId));
    }

    /**
     * @param int $manufacturerId
     * @param int $vehicleType
     * @return VehicleModel[]
     */
    public function getVehicleModels(
        int $manufacturerId,
        int $vehicleType = self::TYPE_PASSENGER
    ): array {
        $tecDocVehicleModels = $this->apiManager->getModelSeries(
            $manufacturerId,
            $this->convertVehicleTypeFlag($vehicleType)
        );

        $vehicleModels = [];
        foreach ($tecDocVehicleModels as $tecDocVehicleModel) {
            $model = VehicleModel::createFromTecDocItem($tecDocVehicleModel);
            $vehicleModels[$model->getId()] = $model;
        }

        return $vehicleModels;
    }

    /**
     * @param int $manufacturerId
     * @param int $modelId
     * @param int $vehicleType
     * @return VehicleModel
     * @throws NotFoundException
     */
    public function getVehicleModelById(
        int $manufacturerId,
        int $modelId,
        int $vehicleType = self::TYPE_PASSENGER
    ): VehicleModel {
        $vehicleModels = $this->getVehicleModels($manufacturerId, $vehicleType);

        if (isset($vehicleModels[$modelId])) {
            return $vehicleModels[$modelId];
        }

        throw new NotFoundException(\sprintf('Vehicle %d model %d does not exist', $manufacturerId, $modelId));
    }

    /**
     * @param int $manufacturerId
     * @param int $modelId
     * @param int $carType
     * @return Vehicle[]
     */
    public function getVehicles(
        int $manufacturerId,
        int $modelId,
        int $carType = self::TYPE_PASSENGER
    ): array {
        $vehiclesData = $this->apiManager->getVehicleIdsByCriteria(
            $manufacturerId,
            $modelId,
            $this->convertVehicleTypeFlag($carType)
        );

        $vehicleIds = [];
        foreach ($vehiclesData as $vehicle) {
            $vehicleIds[$vehicle->carId] = $vehicle->carId;
        }

        $vehicles = [];
        if (\count($vehicleIds) > 0) {
            foreach (\array_chunk($vehicleIds, $this::CHUNK_SIZE) as $chunk) {
                $vehicles[] = $this->getVehiclesByIds($chunk);
            }

            $vehicles = !empty($vehicles) ? \array_merge(...$vehicles) : $vehicles;
        }

        return $vehicles;
    }

    /**
     * @param array $vehicleIds
     * @return Vehicle[]
     */
    public function getVehiclesByIds(array $vehicleIds): array
    {
        $vehicles = [];
        foreach (\array_chunk($vehicleIds, 25) as $ids) {
            $vehicles[] = $this->getVehiclesByIds4($ids);
        }

        return !empty($vehicles) ? \array_merge(...$vehicles) : $vehicles;
    }

    /**
     * @param int[] $vehicleIds
     * @return Vehicle[]
     */
    private function getVehiclesByIds4(array $vehicleIds)
    {
        $vehicles = [];
        foreach ($this->apiManager->getVehicleByIds4($vehicleIds) as $tecDocVehicle) {
            if (!isset($tecDocVehicle->vehicleDetails)) {
                continue;
            }

            $vehicle = Vehicle::createFromTecDocItem(
                $this->transformVehicleDataToVehicle($tecDocVehicle->vehicleDetails)
            );

            $vehicles[$vehicle->getId()] = $vehicle;
        }

        return $vehicles;
    }

    /**
     * @param int $vehicleId
     * @return Vehicle
     * @throws NotFoundException
     */
    public function getVehicleById(int $vehicleId): Vehicle
    {
        $vehicles = $this->getVehiclesByIds([$vehicleId]);
        $vehicle = \current($vehicles);
        if ($vehicle instanceof Vehicle) {
            return $vehicle;
        }

        throw new NotFoundException(\sprintf('Vehicle %d does not exist', $vehicleId));
    }

    /**
     * @param \stdClass $vehicleData
     * @return \stdClass
     */
    protected function transformVehicleDataToVehicle(\stdClass $vehicleData): \stdClass
    {
        $vehicleData->modelDesc = $vehicleData->modelName;
        $vehicleData->cylinderCapacity = $vehicleData->cylinderCapacityCcm ?? null;
        $vehicleData->manuDesc = $vehicleData->manuName;
        $vehicleData->modelId = $vehicleData->modId;
        $vehicleData->carDesc = $vehicleData->typeName;
        $vehicleData->{VehicleModel::DATE_FROM_FIELD_SOURCE_VEHICLE} = $vehicleData->yearOfConstrFrom;
        if (isset($vehicleData->yearOfConstrTo)) {
            $vehicleData->{VehicleModel::DATE_TO_FIELD_SOURCE_VEHICLE} = $vehicleData->yearOfConstrTo;
        }

        return $vehicleData;
    }

    /**
     * @param int $articleId
     * @param string $manufacturerId
     * @return Vehicle[]
     */
    public function getArticleCompatibleVehicles(int $articleId, string $manufacturerId): array
    {
        $pairs = $this->handleArticleVehiclePairs(
            $this->apiManager->getArticleLinkedAllLinkingTarget4($articleId, $manufacturerId)
        );

        $vehicles = [];
        foreach (\array_chunk($pairs, $this::CHUNK_SIZE) as $pairsChunk) {
            $linkedVehicles = $this->apiManager->getArticleLinkedAllLinkingTargetsByIds3($articleId, $pairsChunk);

            /** @var \stdClass $linkedVehicle */
            foreach ($linkedVehicles as $linkedVehicle) {
                if (!isset($linkedVehicle->linkedVehicles->array)) {
                    continue;
                }

                $linkedAttributes = $linkedVehicle->linkedArticleImmediateAttributs;
                $productAttributes = [];

                if (!empty($linkedAttributes)) {
                    $productAttributes = $this->parseProductAttributes($linkedAttributes);
                }

                foreach ($linkedVehicle->linkedVehicles->array as $tecDocVehicle) {
                    $vehicle = Vehicle::createFromTecDocItem($tecDocVehicle);

                    foreach ($productAttributes as $productAttribute) {
                        $vehicle->addProductAttribute($productAttribute);
                    }

                    $vehicles[$vehicle->getId()] = $vehicle;
                }
            }
        }

        return $vehicles;
    }

    /**
     * @param Vehicle $vehicle
     * @param array $articlePairs
     * @return Article[]
     */
    public function getArticlesByVehicle(Vehicle $vehicle, array $articlePairs): array
    {
        $linkingTargetId = $vehicle->getId();
        $manufacturerId = $vehicle->getManufacturer()->getId();
        $modelId = $vehicle->getModel()->getId();

        $articles = [];
        foreach (\array_chunk($articlePairs, self::CHUNK_SIZE) as $chunk) {
            $tecDocArticles = $this->apiManager->getAssignedArticlesByIds7(
                $linkingTargetId,
                $this->vehicleTargetTypeResolver->resolve($vehicle),
                $manufacturerId,
                $modelId,
                $chunk
            );

            $articles[] = \array_map(
                static function (\stdClass $article): Article {
                    $article->directArticle = $article->assignedArticle;

                    return Article::createFromTecDocItem($article);
                },
                $tecDocArticles
            );
        }

        return !empty($articles) ? \array_merge(...$articles) : $articles;
    }

    /**
     * @param array $genericArticleIds
     * @param Vehicle $vehicle
     * @param array $brandNumbers
     * @param Category $category
     * @return Article[]
     */
    public function getArticlesByVehicleAndGenericArticle(
        array $genericArticleIds,
        Vehicle $vehicle,
        array $brandNumbers = [],
        Category $category = null
    ): array {
        $tecDocArticles = $this->apiManager->getArticleIdsWithState(
            $genericArticleIds,
            $vehicle->getId(),
            $this->vehicleTargetTypeResolver->resolve($vehicle),
            $brandNumbers,
            null !== $category ? $category->getId() : null
        );

        $articles = [];
        foreach ($tecDocArticles as $tecDocArticle) {
            $articles[] = Article::createFromSimpleTecDocItem($tecDocArticle);
        }

        return $articles;
    }

    /**
     * @param int $type
     * @return string
     */
    public function convertVehicleTypeFlag(int $type): string
    {
        switch ($type) {
            case self::TYPE_ALL:
                return \sprintf('%s%s', TargetType::PASSENGER_CAR, TargetType::COMMERCIAL_CAR);
            case self::TYPE_COMMERCIAL:
                return TargetType::COMMERCIAL_CAR;
            default:
                return TargetType::PASSENGER_CAR;
        }
    }

    /**
     * @param string $manufacturer
     * @return bool
     */
    protected function isMotorcycle(string $manufacturer): bool
    {
        if (Str::endsWith(Str::lower($manufacturer), ' mc')) {
            return true;
        }

        if (Str::endsWith(Str::lower($manufacturer), 'motorcycle')) {
            return true;
        }

        if (Str::endsWith(Str::lower($manufacturer), 'motorcycles')) {
            return true;
        }

        if (Str::lower($manufacturer) === 'zuendapp') {
            return true;
        }

        return false;
    }

    /**
     * @param \stdClass $linkedArticleImmediateAttributes
     * @return Attribute[]
     */
    protected function parseProductAttributes(\stdClass $linkedArticleImmediateAttributes): array
    {
        $attributes = [];

        if (!isset($linkedArticleImmediateAttributes->array)) {
            return $attributes;
        }

        foreach ($linkedArticleImmediateAttributes->array as $linkedAttribute) {
            $attributes[] = Attribute::createFromTecDocItem($linkedAttribute);
        }

        return $attributes;
    }

    /**
     * @param array $tecDocArticleLinkages
     * @return array
     */
    protected function handleArticleVehiclePairs(array $tecDocArticleLinkages): array
    {
        if (empty($tecDocArticleLinkages)) {
            return [];
        }

        $tecDocArticleLinkages = \reset($tecDocArticleLinkages);
        if (!isset($tecDocArticleLinkages->articleLinkages->array)) {
            return [];
        }

        return \array_map(
            static function (\stdClass $articleLinkage): array {
                return [
                    'articleLinkId' => $articleLinkage->articleLinkId,
                    'linkingTargetId' => $articleLinkage->linkingTargetId,
                ];
            },
            $tecDocArticleLinkages->articleLinkages->array
        );
    }
}
