<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager;

use Nfq\Bundle\TecDocBundle\Entity\FilterValue;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;
use Nfq\Bundle\TecDocBundle\Helpers\ArticleHelper;
use Nfq\Bundle\TecDocBundle\Resolver\VehicleTargetTypeResolver;

class SearchManager
{
    /**
     * @var VehicleTargetTypeResolver
     */
    protected $vehicleTargetTypeResolver;

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * @param VehicleTargetTypeResolver $vehicleTargetTypeResolver
     * @param FilterManager $filterManager
     */
    public function __construct(VehicleTargetTypeResolver $vehicleTargetTypeResolver, FilterManager $filterManager)
    {
        $this->vehicleTargetTypeResolver = $vehicleTargetTypeResolver;
        $this->filterManager = $filterManager;
    }

    /**
     * @param array $allArticles
     * @param Vehicle $vehicle
     * @param int|null $filterGenericArticleId
     * @param FilterValue[] $filterValues
     * @return array
     */
    public function findVehicleArticlesByFilterValues(
        array $allArticles,
        Vehicle $vehicle,
        int $filterGenericArticleId = null,
        array $filterValues = []
    ): array {
        $filters = [];
        if (null !== $filterGenericArticleId) {
            $filtersWithArticleLinks = $this->filterManager->getFiltersWithArticleLinks(
                $filterGenericArticleId,
                $vehicle->getId(),
                $this->vehicleTargetTypeResolver->resolve($vehicle),
                $filterValues,
                ArticleHelper::getArticlesIds($allArticles)
            );

            if (Arr::keyExists(FilterManager::KEY_ARTICLE_LINKS, $filtersWithArticleLinks)) {
                $allArticles = $this->filterArticlesByLinks(
                    $allArticles,
                    $filtersWithArticleLinks[FilterManager::KEY_ARTICLE_LINKS]
                );
            }

            if (Arr::keyExists(FilterManager::KEY_FILTERS, $filtersWithArticleLinks)) {
                $filters = $filtersWithArticleLinks[FilterManager::KEY_FILTERS];
            }
        }

        return [
            $allArticles,
            $filters,
        ];
    }

    /**
     * @param Article[] $articles
     * @param array $articleLinks
     * @return Article[]
     */
    protected function filterArticlesByLinks(array $articles, array $articleLinks): array
    {
        $articleLinksIndex = [];
        foreach ($articleLinks as $articleLink) {
            $articleLinksIndex[$articleLink['articleId']][$articleLink['articleLinkId']] = true;
        }

        foreach ($articles as $key => $article) {
            if (!isset($articleLinksIndex[$article->getId()][$article->getLinkId()])) {
                unset($articles[$key]);
            }
        }

        return $articles;
    }
}
