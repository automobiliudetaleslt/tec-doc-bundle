<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Entity\GenericArticle;
use Nfq\Bundle\TecDocBundle\Exception\InvalidArticleCodeException;
use Nfq\Bundle\TecDocBundle\Helpers\Str;
use Nfq\Bundle\TecDocBundle\ModelManager\Filter\ArticleFilterInterface;
use Nfq\Bundle\TecDocBundle\TecDoc;
use Nfq\Bundle\TecDocBundle\Transformer\TransformerInterface;

class ArticleManager
{
    const CHUNK_SIZE = 25;
    const MIN_CHAR_SEARCH = 4;

    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @var TransformerInterface
     */
    protected $transformer;

    /**
     * @var ArticleFilterInterface
     */
    protected $articleFilter;

    /**
     * @param TecDocApiManagerInterface $apiManager
     * @param TransformerInterface $transformer
     * @param ArticleFilterInterface $articleFilter
     */
    public function __construct(
        TecDocApiManagerInterface $apiManager,
        TransformerInterface $transformer,
        ArticleFilterInterface $articleFilter
    ) {
        $this->apiManager = $apiManager;
        $this->transformer = $transformer;
        $this->articleFilter = $articleFilter;
    }

    /**
     * @param string $code
     * @param int $type
     * @param bool $searchExact
     * @return Article[]
     */
    public function findArticlesByCode(
        string $code,
        int $type = TecDoc::ANY_NUMBER,
        bool $searchExact = true
    ): array {
        try {
            $articles = $this->fetchArticlesByCode($code, $type, $searchExact);
        } catch (InvalidArticleCodeException $e) {
            return [];
        }

        return $this->transformer->transformInitial($articles);
    }

    /**
     * @param string $code
     * @return Article[]
     */
    public function findProductsByOeCode(string $code): array
    {
        try {
            $articles = $this->fetchArticlesByCode($code, TecDoc::OE_NUMBER);
        } catch (InvalidArticleCodeException $e) {
            return [];
        }

        return $this->getProductsFromArticles($articles);
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function findEquipmentByArticleId(int $articleId): array
    {
        return $this->findProductsByArticleIds(
            $this->getArticleIdsFromPartList(
                $this->apiManager->getArticlePartList($articleId)
            )
        );
    }

    /**
     * @param bool $searchTreeNodes
     * @param bool $linked
     * @param int|null $linkingTargetId
     * @param string|null $linkingTargetType
     * @return GenericArticle[]
     */
    public function getGenericArticles(
        bool $searchTreeNodes = true,
        bool $linked = false,
        ?int $linkingTargetId = null,
        ?string $linkingTargetType = null
    ): array {
        return \array_map(
            static function (\stdClass $item) {
                return GenericArticle::createFromTecDocItem($item);
            },
            $this->apiManager->getGenericArticles($searchTreeNodes, $linked, $linkingTargetId, $linkingTargetType)
        );
    }

    /**
     * @param array $partList
     * @return array
     */
    protected function getArticleIdsFromPartList(array $partList): array
    {
        $partList = \reset($partList);
        if (!isset($partList->partlistInfo->array)) {
            return [];
        }

        return \array_map(
            static function ($part) {
                return $part->partlistDetails->partArticleId;
            },
            $partList->partlistInfo->array
        );
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function findAccessoriesByArticleId(int $articleId): array
    {
        return $this->findProductsByArticleIds(
            $this->getArticleIdsFromAccessoryList(
                $this->apiManager->getArticleAccessoryList4($articleId)
            )
        );
    }

    /**
     * @param array $accessoryList
     * @return array
     */
    protected function getArticleIdsFromAccessoryList(array $accessoryList): array
    {
        return \array_map(
            static function ($part) {
                return $part->accessoryDetails->accessoryArticleId;
            },
            $accessoryList
        );
    }

    /**
     * @param array $articleIds
     * @param string|null $language
     * @return Article[]
     */
    public function findProductsByArticleIds(array $articleIds, string $language = null): array
    {
        $articles = [];

        foreach (\array_chunk($articleIds, self::CHUNK_SIZE) as $articleIdsChunk) {
            $articles[] = $this->apiManager->getDirectArticlesByIds7($articleIdsChunk, $language);
        }

        $articles = $this->articleFilter->filter(
            !empty($articles) ? \array_merge(...$articles) : $articles
        );

        return $this->transformArticlesToProducts($articles);
    }

    /**
     * @param array $articles
     * @return array
     */
    protected function getArticleIds(array $articles): array
    {
        return \array_map(
            static function ($article) {
                return $article->articleId;
            },
            $articles
        );
    }

    /**
     * @param string $code
     * @param int $type
     * @param bool $searchExact
     * @return array
     * @throws InvalidArticleCodeException
     */
    protected function fetchArticlesByCode(string $code, int $type, bool $searchExact = true): array
    {
        $this->validateCode($code);

        return $this->apiManager->getArticleDirectSearchAllNumbersWithState($code, $type, $searchExact);
    }

    /**
     * @param array $articles
     * @return Article[]
     */
    protected function getProductsFromArticles(array $articles): array
    {
        return $this->findProductsByArticleIds(
            $this->getArticleIds($articles)
        );
    }

    /**
     * @param array $articles
     * @return Article[]
     */
    protected function transformArticlesToProducts(array $articles): array
    {
        return $this->transformer->transformMultiple($articles);
    }

    /**
     * @param string $code
     * @throws InvalidArticleCodeException
     */
    protected function validateCode(string $code): void
    {
        if (Str::length($code) < self::MIN_CHAR_SEARCH) {
            throw new InvalidArticleCodeException(
                \sprintf(
                    'Field "articleNumber" must contain at least %s alphanumeric characters for a similar search.',
                    self::MIN_CHAR_SEARCH
                )
            );
        }
    }
}
