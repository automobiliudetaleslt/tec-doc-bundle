<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager\Model;

final class ArticleAttributes
{
    const BRAND = 'brand';
    const BRAND_PRODUCT_CODE = 'brand_product_code';
    const PRODUCT_CODE = 'product_code';

    const MOUNTING_PLACE_ID = 100;

    const METADATA_ATTRIBUTES = [
        self::BRAND,
        self::BRAND_PRODUCT_CODE,
        self::PRODUCT_CODE,
    ];
}
