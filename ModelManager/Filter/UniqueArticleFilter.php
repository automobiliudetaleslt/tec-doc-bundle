<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager\Filter;

class UniqueArticleFilter implements ArticleFilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function filter(array $articles): array
    {
        $articleIds = [];
        $filteredArticles = [];

        foreach ($articles as $article) {
            if (!isset($article->directArticle)) {
                continue;
            }

            $articleId = $article->directArticle->articleId;

            if (!\in_array($articleId, $articleIds)) {
                $articleIds[] = $articleId;
                $filteredArticles[] = $article;
            }
        }

        return $filteredArticles;
    }
}
