<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Builder\CategoriesTreeBuilder;
use Nfq\Bundle\TecDocBundle\Entity\Collections\CategoriesTree;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;
use Nfq\Bundle\TecDocBundle\TargetType;

class CategoryManager
{
    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @var CategoriesTreeBuilder
     */
    protected $categoriesTreeBuilder;

    /**
     * @var CategoriesTree[]
     */
    private $trees = [];

    /**
     * @param TecDocApiManagerInterface $apiManager
     * @param CategoriesTreeBuilder $categoriesTreeBuilder
     */
    public function __construct(TecDocApiManagerInterface $apiManager, CategoriesTreeBuilder $categoriesTreeBuilder)
    {
        $this->apiManager = $apiManager;
        $this->categoriesTreeBuilder = $categoriesTreeBuilder;
    }

    /**
     * @param string|null $linkingTargetId
     * @param string $linkingTargetType
     * @param int|null $parentId
     * @param bool $includeChildNodes
     * @return CategoriesTree
     */
    public function getCategoriesTree(
        string $linkingTargetId = null,
        string $linkingTargetType = TargetType::PASSENGER_CAR,
        int $parentId = null,
        bool $includeChildNodes = true
    ): CategoriesTree {
        $cacheKey = \md5(
            \sprintf(
                '%s-%s-%s-%s',
                $linkingTargetId ?? 'null',
                $linkingTargetType,
                $parentId ?? 'null',
                $includeChildNodes ? 'true' : 'false'
            )
        );

        if (!Arr::keyExists($cacheKey, $this->trees)) {
            $nodes = $this->apiManager->getChildNodesAllLinkingTarget2(
                $linkingTargetType,
                $linkingTargetId,
                $parentId,
                $includeChildNodes
            );

            $this->trees[$cacheKey] = $this->categoriesTreeBuilder->buildCategoriesTree($nodes);
        }

        return $this->trees[$cacheKey];
    }
}
