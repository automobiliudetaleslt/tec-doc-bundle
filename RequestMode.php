<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle;

final class RequestMode
{
    /**
     * Available criteria request modes
     */
    public const ARTICLES_AND_ATTRIBUTES = '3';

    /**
     * Forbid class initialization.
     */
    private function __construct()
    {
    }
}
