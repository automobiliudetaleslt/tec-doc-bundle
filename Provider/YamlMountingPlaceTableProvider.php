<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider;

use Nfq\Bundle\TecDocBundle\Loader\YamlMountingPlaceFileLoader;
use Nfq\Bundle\TecDocBundle\Model\PartMountingPlaceTable;

class YamlMountingPlaceTableProvider implements MountingPlaceTableProviderInterface
{
    /**
     * @var YamlMountingPlaceFileLoader
     */
    protected $yamlFileLoader;

    /**
     * @param YamlMountingPlaceFileLoader $fileLoader
     */
    public function __construct(YamlMountingPlaceFileLoader $fileLoader)
    {
        $this->yamlFileLoader = $fileLoader;
    }

    /**
     * @return PartMountingPlaceTable
     */
    public function getPartMountingPlaceTable(): PartMountingPlaceTable
    {
        return $this->buildMountingPlaceTable($this->yamlFileLoader->getFileData());
    }

    /**
     * @param array $yamlParsedData
     * @return PartMountingPlaceTable
     */
    protected function buildMountingPlaceTable(array $yamlParsedData): PartMountingPlaceTable
    {
        $mountingPlaceTable = new PartMountingPlaceTable();

        $mountingPlaces = $yamlParsedData['mounting_places'];

        foreach ($mountingPlaces as $locale => $mountingPlacesGroups) {
            foreach ($mountingPlacesGroups as $key => $mountingPlace) {
                $mountingPlaceTable->setMountingPlace($locale, $key, $mountingPlace);
            }
        }

        return $mountingPlaceTable;
    }
}
