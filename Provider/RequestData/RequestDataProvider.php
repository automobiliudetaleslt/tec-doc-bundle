<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider\RequestData;

use Nfq\Bundle\TecDocBundle\Provider\RequestData\Method\MethodRequestDataProviderInterface;

class RequestDataProvider implements RequestDataProviderInterface
{
    /**
     * @var MethodRequestDataProviderInterface[]
     */
    private $providers = [];

    /**
     * @param MethodRequestDataProviderInterface $provider
     */
    public function addProvider(MethodRequestDataProviderInterface $provider)
    {
        if (!\in_array($provider, $this->providers, true)) {
            $this->providers[] = $provider;
        }
    }

    /**
     * @param string $method
     * @return array
     */
    public function getRequestData(string $method): array
    {
        $requestData = [];
        foreach ($this->providers as $provider) {
            if ($provider->providesForMethod($method)) {
                $requestData = \array_merge($requestData, $provider->getRequestData($method));
            }
        }

        return $requestData;
    }
}
