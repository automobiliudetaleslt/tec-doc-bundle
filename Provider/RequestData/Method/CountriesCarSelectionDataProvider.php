<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider\RequestData\Method;

class CountriesCarSelectionDataProvider implements MethodRequestDataProviderInterface
{
    private const PROVIDES_FOR_METHODS = [
        'getVehicleIdsByCriteria',
        'getVehicleByIds4',
    ];

    /**
     * @var string
     */
    private $articleCountry;

    /**
     * @param string $articleCountry
     */
    public function __construct(string $articleCountry)
    {
        $this->articleCountry = $articleCountry;
    }

    /**
     * {@inheritdoc}
     */
    public function providesForMethod(string $method): bool
    {
        return \in_array($method, self::PROVIDES_FOR_METHODS);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(string $method): array
    {
        return ['countriesCarSelection' => $this->articleCountry];
    }
}
