<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider;

use Symfony\Component\Routing\RouterInterface;

class PathProvider
{
    const IMAGE_FLAG_NORMAL = 0;
    const IMAGE_FLAG_THUMBNAIL = 1;

    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var int
     */
    protected $providerId;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param string $basePath
     * @param int $providerId
     * @param RouterInterface $router
     */
    public function __construct(string $basePath, int $providerId, RouterInterface $router)
    {
        $this->basePath = $basePath;
        $this->providerId = $providerId;
        $this->router = $router;
    }

    /**
     * @param int $id
     * @return string
     */
    public function getDocumentPath(int $id): string
    {
        return \sprintf('%s/%s/%s/0', $this->basePath, $this->providerId, $id);
    }

    /**
     * @param int $thumbnailId
     * @param int $typeId
     * @return string
     */
    public function getThumbnailPath(int $thumbnailId, int $typeId): string
    {
        return \sprintf('%s/%s/%s/%s', $this->basePath, $this->providerId, $thumbnailId, $typeId);
    }

    /**
     * @param int $thumbnailId
     * @param int $typeId
     * @return string
     */
    public function getInternalThumbnailPath(int $thumbnailId, int $typeId): string
    {
        return $this->router->generate(
            'nfq_product.thumbnail',
            [
                'id' => $thumbnailId,
                'typeId' => $typeId,
            ]
        );
    }
}
