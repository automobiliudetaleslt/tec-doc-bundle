<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ParamConverter;

use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\Exception\NotFoundException;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VehicleManufacturerParamConverter implements ParamConverterInterface
{
    /**
     * @var VehicleManager
     */
    protected $vehicleManager;

    /**
     * @param VehicleManager $vehicleManager
     */
    public function __construct(VehicleManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $name = $configuration->getName();
        $manufacturerId = $request->attributes->get($name);
        if (null === $manufacturerId) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        try {
            $vehicleManufacturer = $this->vehicleManager->getVehicleManufacturerById(
                $manufacturerId,
                VehicleManager::TYPE_ALL
            );
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        $request->attributes->set($name, $vehicleManufacturer);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return VehicleManufacturer::class === $configuration->getClass();
    }
}
