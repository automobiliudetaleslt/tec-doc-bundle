<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ParamConverter;

use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;
use Nfq\Bundle\TecDocBundle\Exception\NotFoundException;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VehicleModelParamConverter implements ParamConverterInterface
{
    /**
     * @var VehicleManager
     */
    protected $vehicleManager;

    /**
     * @param VehicleManager $vehicleManager
     */
    public function __construct(VehicleManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $vehicleManufacturerId = $this->getVehicleManufacturerId($request, $configuration);
        if (null === $vehicleManufacturerId) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        $name = $configuration->getName();
        $vehicleModelId = $request->attributes->get($name);
        if (null === $vehicleModelId) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        try {
            $vehicleModel = $this->vehicleManager->getVehicleModelById($vehicleManufacturerId, $vehicleModelId);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        $request->attributes->set($name, $vehicleModel);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return VehicleModel::class === $configuration->getClass();
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return int|null
     */
    private function getVehicleManufacturerId(Request $request, ParamConverter $configuration): ?int
    {
        $options = $configuration->getOptions();
        if (!isset($options['vehicleManufacturer']) || !$options['vehicleManufacturer']) {
            return null;
        }

        $vehicleManufacturer = $request->attributes->get($options['vehicleManufacturer']);

        if ($vehicleManufacturer instanceof VehicleManufacturer) {
            return $vehicleManufacturer->getId();
        }

        return (int)$vehicleManufacturer;
    }
}
