<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ParamConverter;

use Nfq\Bundle\TecDocBundle\Entity\Category;
use Nfq\Bundle\TecDocBundle\Exception\CategoryNotFoundException;
use Nfq\Bundle\TecDocBundle\ModelManager\CategoryManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryParamConverter implements ParamConverterInterface
{
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @param CategoryManager $categoryManager
     */
    public function __construct(CategoryManager $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $name = $configuration->getName();
        $categorySlug = $request->attributes->get($name, '');

        try {
            $category = $this->categoryManager->getCategoriesTree()->findOneBySlug($categorySlug);
        } catch (CategoryNotFoundException $e) {
            throw new NotFoundHttpException(\sprintf('Category "%s" was not found', $categorySlug));
        }

        $request->attributes->set($name, $category);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return Category::class === $configuration->getClass();
    }
}
