<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ApiManager;

use GuzzleHttp\Command\CommandInterface;
use Nfq\Bundle\GuzzleConfigBundle\Client\GuzzleClient;
use Nfq\Bundle\GuzzleConfigBundle\Manager\BaseApiManager;
use Nfq\Bundle\TecDocBundle\Entity\GenericArticleByManufacturer6;
use Nfq\Bundle\TecDocBundle\Generator\RequestDataGenerator;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;
use Nfq\Bundle\TecDocBundle\Model\TecDocResponse;
use Nfq\Bundle\TecDocBundle\Provider\MethodTimeoutProvider;
use Nfq\Bundle\TecDocBundle\ResultMode;
use Nfq\Bundle\TecDocBundle\SortMode;
use Nfq\Bundle\TecDocBundle\TargetType;
use Psr\Log\LoggerInterface;

class TecDocApiManager extends BaseApiManager implements TecDocApiManagerInterface
{
    /**
     * @var RequestDataGenerator
     */
    protected $requestDataGenerator;

    /**
     * @var MethodTimeoutProvider
     */
    protected $methodTimeoutProvider;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param GuzzleClient $client
     * @param RequestDataGenerator $requestDataGenerator
     * @param MethodTimeoutProvider $methodTimeoutProvider
     * @param LoggerInterface $logger
     */
    public function __construct(
        GuzzleClient $client,
        RequestDataGenerator $requestDataGenerator,
        MethodTimeoutProvider $methodTimeoutProvider,
        LoggerInterface $logger
    ) {
        parent::__construct($client);

        $this->requestDataGenerator = $requestDataGenerator;
        $this->methodTimeoutProvider = $methodTimeoutProvider;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleDirectSearchAllNumbersWithState(
        string $articleNumber,
        int $numberType,
        bool $searchExact = true
    ): array {
        return $this->call(
            __FUNCTION__,
            ['articleNumber' => $articleNumber, 'numberType' => $numberType, 'searchExact' => $searchExact]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleIdsWithState(
        array $genericArticleIds,
        int $linkingTargetId,
        string $linkingTargetType,
        array $brandNumbers = [],
        int $assemblyGroupNodeId = null
    ): array {
        $arguments = [
            'linkingTargetId' => $linkingTargetId,
            'linkingTargetType' => $linkingTargetType,
        ];

        if (!empty($genericArticleIds)) {
            $arguments['genericArticleId'] = ['array' => \array_values($genericArticleIds)];
        }

        if (!empty($brandNumbers)) {
            $arguments['brandNo'] = ['array' => \array_values($brandNumbers)];
        }

        if (null !== $assemblyGroupNodeId) {
            $arguments['assemblyGroupNodeId'] = $assemblyGroupNodeId;
        }

        return $this->call(__FUNCTION__, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectArticlesByIds7(array $articleIds, string $language = null): array
    {
        $arguments = [
            'articleId' => [
                'array' => \array_values($articleIds),
            ],
            'basicData' => true,
            'attributs' => true,
            'info' => true,
            'thumbnails' => true,
            'eanNumbers' => true,
            'oeNumbers' => true,
            'documents' => true,
            'replacedByNumbers' => true,
        ];

        if (null !== $language) {
            $arguments['lang'] = $language;
        }

        return $this->call(__FUNCTION__, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignedArticlesByIds7(
        int $linkingTargetId,
        string $linkingTargetType,
        int $manufacturerId,
        int $modelId,
        array $articlePairs
    ): array {
        return $this->call(
            __FUNCTION__,
            [
                'articleIdPairs' => ['array' => \array_values($articlePairs)],
                'linkingTargetType' => $linkingTargetType,
                'linkingTargetId' => $linkingTargetId,
                'manuId' => $manufacturerId,
                'modId' => $modelId,
                'basicData' => true,
                'thumbnails' => true,
                'eanNumbers' => true,
                'attributs' => true,
                'oeNumbers' => true,
                'documents' => true,
                'replacedByNumbers' => true,
                'info' => true,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getArticlePartList(int $articleId): array
    {
        return $this->call(
            __FUNCTION__,
            [
                'articleId' => $articleId,
                // Only one linking target type value is supported by TecDoc
                'linkingTargetType' => TargetType::PASSENGER_CAR,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticles(
        bool $searchTreeNodes = true,
        bool $linked = false,
        ?int $linkingTargetId = null,
        ?string $linkingTargetType = null
    ): array {
        $arguments = [
            'searchTreeNodes' => $searchTreeNodes,
            'linked' => $linked,
        ];

        if ($linked) {
            $arguments['linkingTargetId'] = $linkingTargetId;
            $arguments['linkingTargetType'] = $linkingTargetType;
        }

        return $this->call(__FUNCTION__, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleAccessoryList4(int $articleId): array
    {
        return $this->call(__FUNCTION__, ['articleId' => $articleId]);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetManufacturer2(int $articleId): array
    {
        return $this->call(
            __FUNCTION__,
            [
                'articleId' => $articleId,
                'linkingTargetType' => \sprintf('%s%s', TargetType::COMMERCIAL_CAR, TargetType::PASSENGER_CAR),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTarget4(int $articleId, string $manufacturerId): array
    {
        return $this->call(
            __FUNCTION__,
            [
                'articleId' => $articleId,
                'linkingTargetType' => \sprintf('%s%s', TargetType::COMMERCIAL_CAR, TargetType::PASSENGER_CAR),
                'linkingTargetManuId' => $manufacturerId,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetsByIds3(int $articleId, array $linkedArticlePairs): array
    {
        return $this->call(
            __FUNCTION__,
            [
                'articleId' => $articleId,
                'linkingTargetType' => \sprintf('%s%s', TargetType::COMMERCIAL_CAR, TargetType::PASSENGER_CAR),
                'immediateAttributs' => true,
                'linkedArticlePairs' => ['array' => \array_values($linkedArticlePairs)],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getManufacturers(string $linkingTargetType = TargetType::PASSENGER_CAR): array
    {
        return $this->call(__FUNCTION__, ['linkingTargetType' => $linkingTargetType]);
    }

    /**
     * {@inheritdoc}
     */
    public function getModelSeries(int $manufacturerId, string $linkingTargetType): array
    {
        return $this->call(
            __FUNCTION__,
            [
                'manuId' => $manufacturerId,
                'linkingTargetType' => $linkingTargetType,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleIdsByCriteria(
        int $manufacturerId,
        int $modelId,
        string $carType = TargetType::PASSENGER_CAR
    ): array {
        return $this->call(
            __FUNCTION__,
            [
                'carType' => $carType,
                'manuId' => $manufacturerId,
                'modId' => $modelId,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleByIds4(array $vehicleIds): array
    {
        return $this->call(__FUNCTION__, ['carIds' => ['array' => \array_values($vehicleIds)]]);
    }

    /**
     * {@inheritdoc}
     */
    public function getCriteriaDialogAttributs(
        int $genericArticleId,
        int $linkingTargetId,
        string $linkingTargetType,
        string $mode,
        array $attributeValues = [],
        array $articleIds = []
    ): array {
        $articleIdsPairs = [];
        foreach ($articleIds as $articleId) {
            $articleIdsPairs[] = ['articleId' => $articleId];
        }

        $arguments = [
            'genericArticleId' => $genericArticleId,
            'linkingTargetType' => $linkingTargetType,
            'linkingTargetId' => $linkingTargetId,
            'requestMode' => $mode,
        ];

        if (!empty($attributeValues)) {
            $arguments['attributeValues'] = ['array' => \array_values($attributeValues)];
        }

        if (!empty($articleIdsPairs)) {
            $arguments['articleIds'] = ['array' => \array_values($articleIdsPairs)];
        }

        return $this->call(__FUNCTION__, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function getChildNodesAllLinkingTarget2(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $parentId = null,
        bool $includeChildNodes = true
    ): array {
        return $this->call(
            __FUNCTION__,
            [
                'linkingTargetType' => $linkingTargetType,
                'linkingTargetId' => $linkingTargetId,
                'parentNodeId' => $parentId,
                'childNodes' => $includeChildNodes,
                'linked' => null !== $linkingTargetId,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticlesByManufacturer6(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $assemblyGroupNodeId = null,
        array $brandNumbers = [],
        array $genericArticleId = [],
        int $resultMode = ResultMode::DISTINCT_GENERIC_ARTICLES,
        int $sortMode = SortMode::BRAND_NAME
    ): array {
        $arguments = [
            'linkingTargetType' => $linkingTargetType,
            'resultMode' => $resultMode,
            'sortMode' => $sortMode,
        ];

        if (null !== $linkingTargetId) {
            $arguments['linkingTargetId'] = $linkingTargetId;
        }

        if (null !== $assemblyGroupNodeId) {
            $arguments['assemblyGroupNodeId'] = $assemblyGroupNodeId;
        }

        if (!empty($brandNumbers)) {
            $arguments['brandNo'] = ['array' => \array_values($brandNumbers)];
        }

        if (!empty($genericArticleId)) {
            $arguments['genericArticleId'] = ['array' => \array_values($genericArticleId)];
        }

        return \array_map(
            function (\stdClass $item) {
                return GenericArticleByManufacturer6::createFromTecDocItem($item);
            },
            $this->call(__FUNCTION__, $arguments)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function call(string $commandName, array $arguments = [])
    {
        if (null !== $methodTimeout = $this->methodTimeoutProvider->getTimeout($commandName)) {
            $arguments['@http'] = ['timeout' => $methodTimeout];
        }

        $requestOptions = [];
        if (Arr::keyExists('@http', $arguments)) {
            $requestOptions = (array)$arguments['@http'];
            unset($arguments['@http']);
        }

        $response = parent::call(
            $commandName,
            $this->requestDataGenerator->generate($commandName, $arguments, $requestOptions)
        );

        return $response instanceof TecDocResponse ? $response->getData() : [];
    }

    /**
     * {@inheritdoc}
     */
    protected function handleResponseException(\Exception $exception, CommandInterface $command)
    {
        $exceptionMessage = $exception->getMessage();

        $previous = $exception->getPrevious();
        if (null !== $previous) {
            $exceptionMessage .= \PHP_EOL . $previous->getMessage();
        }

        $this->logger->error(\sprintf('Failed tec doc request: %s', $exceptionMessage));

        return false;
    }
}
