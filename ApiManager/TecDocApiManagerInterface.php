<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ApiManager;

use Nfq\Bundle\TecDocBundle\Entity\GenericArticleByManufacturer6;
use Nfq\Bundle\TecDocBundle\ResultMode;
use Nfq\Bundle\TecDocBundle\SortMode;
use Nfq\Bundle\TecDocBundle\TargetType;

interface TecDocApiManagerInterface
{
    /**
     * @param string $articleNumber
     * @param int $numberType
     * @param bool $searchExact
     * @return array
     */
    public function getArticleDirectSearchAllNumbersWithState(
        string $articleNumber,
        int $numberType,
        bool $searchExact = true
    ): array;

    /**
     * @param string[] $genericArticleIds
     * @param int $linkingTargetId
     * @param string $linkingTargetType
     * @param array $brandNumbers
     * @param int|null $assemblyGroupNodeId
     * @return array
     */
    public function getArticleIdsWithState(
        array $genericArticleIds,
        int $linkingTargetId,
        string $linkingTargetType,
        array $brandNumbers = [],
        int $assemblyGroupNodeId = null
    ): array;

    /**
     * @param int[] $articleIds
     * @param string|null $language
     * @return array
     */
    public function getDirectArticlesByIds7(array $articleIds, string $language = null): array;

    /**
     * @param int $linkingTargetId
     * @param string $linkingTargetType
     * @param int $manufacturerId
     * @param int $modelId
     * @param array $articlePairs
     * @return array
     */
    public function getAssignedArticlesByIds7(
        int $linkingTargetId,
        string $linkingTargetType,
        int $manufacturerId,
        int $modelId,
        array $articlePairs
    ): array;

    /**
     * @param int $articleId
     * @return array
     */
    public function getArticlePartList(int $articleId): array;

    /**
     * @param bool $searchTreeNodes
     * @param bool $linked
     * @param int|null $linkingTargetId
     * @param string|null $linkingTargetType
     * @return array
     */
    public function getGenericArticles(
        bool $searchTreeNodes = true,
        bool $linked = false,
        ?int $linkingTargetId = null,
        ?string $linkingTargetType = null
    ): array;

    /**
     * @param int $articleId
     * @return array
     */
    public function getArticleAccessoryList4(int $articleId): array;

    /**
     * Get article compatible vehicle manufacturers
     *
     * @param int $articleId
     * @return array
     */
    public function getArticleLinkedAllLinkingTargetManufacturer2(int $articleId): array;

    /**
     * Get article compatible vehicle models
     *
     * @param int $articleId
     * @param string $manufacturerId
     * @return array
     */
    public function getArticleLinkedAllLinkingTarget4(int $articleId, string $manufacturerId): array;

    /**
     * Get article compatible vehicle modifications
     *
     * @param int $articleId
     * @param array $linkedArticlePairs
     * @return array
     */
    public function getArticleLinkedAllLinkingTargetsByIds3(int $articleId, array $linkedArticlePairs): array;

    /**
     * @param string $linkingTargetType
     * @return array
     */
    public function getManufacturers(string $linkingTargetType = TargetType::PASSENGER_CAR): array;

    /**
     * @param int $manufacturerId
     * @param string $linkingTargetType
     * @return array
     */
    public function getModelSeries(int $manufacturerId, string $linkingTargetType): array;

    /**
     * @param int $manufacturerId
     * @param int $modelId
     * @param string $carType
     * @return array
     */
    public function getVehicleIdsByCriteria(
        int $manufacturerId,
        int $modelId,
        string $carType = TargetType::PASSENGER_CAR
    ): array;

    /**
     * @param array $vehicleIds
     * @return array
     */
    public function getVehicleByIds4(array $vehicleIds): array;

    /**
     * @param int $genericArticleId
     * @param int $linkingTargetId
     * @param string $linkingTargetType
     * @param string $mode
     * @param array $attributeValues
     * @param array $articleIds
     * @return array
     */
    public function getCriteriaDialogAttributs(
        int $genericArticleId,
        int $linkingTargetId,
        string $linkingTargetType,
        string $mode,
        array $attributeValues = [],
        array $articleIds = []
    ): array;

    /**
     * @param string $linkingTargetType
     * @param string|null $linkingTargetId
     * @param int|null $parentId
     * @param bool $includeChildNodes
     * @return array
     */
    public function getChildNodesAllLinkingTarget2(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $parentId = null,
        bool $includeChildNodes = true
    ): array;

    /**
     * @param string $linkingTargetType
     * @param string|null $linkingTargetId
     * @param int|null $assemblyGroupNodeId
     * @param array $brandNumbers
     * @param array $genericArticleId
     * @param int $resultMode
     * @param int $sortMode
     * @return GenericArticleByManufacturer6[]
     */
    public function getGenericArticlesByManufacturer6(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $assemblyGroupNodeId = null,
        array $brandNumbers = [],
        array $genericArticleId = [],
        int $resultMode = ResultMode::DISTINCT_GENERIC_ARTICLES,
        int $sortMode = SortMode::BRAND_NAME
    ): array;
}
