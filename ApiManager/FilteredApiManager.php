<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ApiManager;

use Nfq\Bundle\TecDocBundle\ResultMode;
use Nfq\Bundle\TecDocBundle\SortMode;
use Nfq\Bundle\TecDocBundle\TargetType;

class FilteredApiManager implements TecDocApiManagerInterface
{
    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @param TecDocApiManagerInterface $apiManager
     */
    public function __construct(TecDocApiManagerInterface $apiManager)
    {
        $this->apiManager = $apiManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleDirectSearchAllNumbersWithState(
        string $articleNumber,
        int $numberType,
        bool $searchExact = true
    ): array {
        $result = $this->apiManager->getArticleDirectSearchAllNumbersWithState(
            $articleNumber,
            $numberType,
            $searchExact
        );

        $filteredResult = [];
        foreach ($result as $item) {
            if (!isset($item->articleNo, $item->brandNo)) {
                $filteredResult[] = $item;

                continue;
            }

            $key = \sprintf('%s-%s', $item->articleNo, $item->brandNo);
            $filteredResult[$key] = $item;
        }

        return \array_values($filteredResult);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleIdsWithState(
        array $genericArticleIds,
        int $linkingTargetId,
        string $linkingTargetType,
        array $brandNumbers = [],
        int $assemblyGroupNodeId = null
    ): array {
        $result = $this->apiManager->getArticleIdsWithState(
            $genericArticleIds,
            $linkingTargetId,
            $linkingTargetType,
            $brandNumbers,
            $assemblyGroupNodeId
        );

        $filteredResult = [];
        foreach ($result as $item) {
            if (!isset($item->articleNo, $item->brandNo)) {
                $filteredResult[] = $item;

                continue;
            }

            $key = \sprintf('%s-%s', $item->articleNo, $item->brandNo);
            $filteredResult[$key] = $item;
        }

        return \array_values($filteredResult);
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectArticlesByIds7(array $articleIds, string $language = null): array
    {
        $result = $this->apiManager->getDirectArticlesByIds7($articleIds, $language);

        $filteredResult = [];
        foreach ($result as $item) {
            if (!isset($item->directArticle, $item->directArticle->articleNo, $item->directArticle->brandNo)) {
                $filteredResult[] = $item;

                continue;
            }

            $key = \sprintf('%s-%s', $item->directArticle->articleNo, $item->directArticle->brandNo);
            $filteredResult[$key] = $item;
        }

        return \array_values($filteredResult);
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignedArticlesByIds7(
        int $linkingTargetId,
        string $linkingTargetType,
        int $manufacturerId,
        int $modelId,
        array $articlePairs
    ): array {
        $result = $this->apiManager->getAssignedArticlesByIds7(
            $linkingTargetId,
            $linkingTargetType,
            $manufacturerId,
            $modelId,
            $articlePairs
        );

        $filteredResult = [];
        foreach ($result as $item) {
            if (!isset($item->assignedArticle, $item->assignedArticle->articleNo, $item->assignedArticle->brandNo)) {
                $filteredResult[] = $item;

                continue;
            }

            $key = \sprintf('%s-%s', $item->assignedArticle->articleNo, $item->assignedArticle->brandNo);
            $filteredResult[$key] = $item;
        }

        return \array_values($filteredResult);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticlePartList(int $articleId): array
    {
        return $this->apiManager->getArticlePartList($articleId);
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticles(
        bool $searchTreeNodes = true,
        bool $linked = false,
        ?int $linkingTargetId = null,
        ?string $linkingTargetType = null
    ): array {
        return $this->apiManager->getGenericArticles($searchTreeNodes, $linked, $linkingTargetId, $linkingTargetType);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleAccessoryList4(int $articleId): array
    {
        return $this->apiManager->getArticleAccessoryList4($articleId);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetManufacturer2(int $articleId): array
    {
        return $this->apiManager->getArticleLinkedAllLinkingTargetManufacturer2($articleId);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTarget4(int $articleId, string $manufacturerId): array
    {
        return $this->apiManager->getArticleLinkedAllLinkingTarget4($articleId, $manufacturerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetsByIds3(int $articleId, array $linkedArticlePairs): array
    {
        return $this->apiManager->getArticleLinkedAllLinkingTargetsByIds3($articleId, $linkedArticlePairs);
    }

    /**
     * {@inheritdoc}
     */
    public function getManufacturers(string $linkingTargetType = TargetType::PASSENGER_CAR): array
    {
        return $this->apiManager->getManufacturers($linkingTargetType);
    }

    /**
     * {@inheritdoc}
     */
    public function getModelSeries(int $manufacturerId, string $linkingTargetType): array
    {
        return $this->apiManager->getModelSeries($manufacturerId, $linkingTargetType);
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleIdsByCriteria(
        int $manufacturerId,
        int $modelId,
        string $carType = TargetType::PASSENGER_CAR
    ): array {
        return $this->apiManager->getVehicleIdsByCriteria($manufacturerId, $modelId, $carType);
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleByIds4(array $vehicleIds): array
    {
        return $this->apiManager->getVehicleByIds4($vehicleIds);
    }

    /**
     * {@inheritdoc}
     */
    public function getCriteriaDialogAttributs(
        int $genericArticleId,
        int $linkingTargetId,
        string $linkingTargetType,
        string $mode,
        array $attributeValues = [],
        array $articleIds = []
    ): array {
        return $this->apiManager->getCriteriaDialogAttributs(
            $genericArticleId,
            $linkingTargetId,
            $linkingTargetType,
            $mode,
            $attributeValues,
            $articleIds
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getChildNodesAllLinkingTarget2(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $parentId = null,
        bool $includeChildNodes = true
    ): array {
        return $this->apiManager->getChildNodesAllLinkingTarget2(
            $linkingTargetType,
            $linkingTargetId,
            $parentId,
            $includeChildNodes
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticlesByManufacturer6(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $assemblyGroupNodeId = null,
        array $brandNumbers = [],
        array $genericArticleId = [],
        int $resultMode = ResultMode::DISTINCT_GENERIC_ARTICLES,
        int $sortMode = SortMode::BRAND_NAME
    ): array {
        return $this->apiManager->getGenericArticlesByManufacturer6(
            $linkingTargetType,
            $linkingTargetId,
            $assemblyGroupNodeId,
            $brandNumbers,
            $genericArticleId,
            $resultMode,
            $sortMode
        );
    }
}
