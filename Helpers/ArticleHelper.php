<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Helpers;

use Nfq\Bundle\TecDocBundle\Entity\Article;

class ArticleHelper
{
    /**
     * @param Article[] $articles
     * @return array
     */
    public static function aggregateArticleLinks(array $articles): array
    {
        $articleLinks = [];
        foreach ($articles as $article) {
            $articleLinks[] = [
                'articleId' => $article->getId(),
                'articleLinkId' => $article->getLinkId(),
            ];
        }

        return $articleLinks;
    }

    /**
     * @param Article[] $articles
     * @return int[]
     */
    public static function getArticlesIds(array $articles): array
    {
        $articlesIds = [];
        foreach ($articles as $article) {
            $articlesIds[] = $article->getId();
        }

        return $articlesIds;
    }

    /**
     * @param Article|null $article1
     * @param Article|null $article2
     * @return bool
     */
    public static function isArticlesIdentical(Article $article1 = null, Article $article2 = null): bool
    {
        // If one of articles is null, but not both
        if (null === $article1 xor null === $article2) {
            return false;
        }

        // If both articles are null
        if (null === $article1 && null === $article2) {
            return true;
        }

        $article1Id = null !== $article1 ? $article1->getId() : null;
        $article2Id = null !== $article2 ? $article2->getId() : null;

        $article1LinkId = null !== $article1 ? $article1->getLinkId() : null;
        $article2LinkId = null !== $article2 ? $article2->getLinkId() : null;

        return (($article1Id === $article2Id) && ($article1LinkId === $article2LinkId));
    }
}
