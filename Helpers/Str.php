<?php

declare(strict_types=1);

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Helpers;

class Str
{
    /**
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    public static function endsWith(string $haystack, $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if ((string)$needle === static::substr($haystack, -static::length($needle))) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param int $offset
     * @return int|false
     */
    public static function pos(string $haystack, string $needle, int $offset = 0)
    {
        return \mb_strpos($haystack, $needle, $offset, 'UTF-8');
    }

    /**
     * @param string $string
     * @param int $start
     * @param int|null $length
     * @return string
     */
    public static function substr(string $string, int $start, int $length = null): string
    {
        return \mb_substr($string, $start, $length, 'UTF-8');
    }

    /**
     * @param string $string
     * @return string
     */
    public static function ucfirst(string $string): string
    {
        return static::upper(static::substr($string, 0, 1)) . static::substr($string, 1);
    }

    /**
     * @param string $value
     * @return string
     */
    public static function upper(string $value): string
    {
        return \mb_strtoupper($value, 'UTF-8');
    }

    /**
     * @param string $value
     * @return string
     */
    public static function lower(string $value): string
    {
        return \mb_strtolower($value, 'UTF-8');
    }

    /**
     * @param string $value
     * @return int
     */
    public static function length(string $value): int
    {
        return \mb_strlen($value);
    }

    /**
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    public static function contains(string $haystack, $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && self::pos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $value
     * @param int $limit
     * @param string $end
     * @return string
     */
    public static function limit(string $value, int $limit = 100, string $end = '...'): string
    {
        if (\mb_strwidth($value, 'UTF-8') <= $limit) {
            return $value;
        }

        return \rtrim(\mb_strimwidth($value, 0, $limit, '', 'UTF-8')) . $end;
    }

    /**
     * @param string $string1
     * @param string $string2
     * @return int
     */
    public static function compare(string $string1, string $string2): int
    {
        return \strcmp($string1, $string2);
    }

    /**
     * @param string $string
     * @return float
     */
    public static function toFloat(string $string): float
    {
        return (float)(\preg_replace('/[^-0-9\.]/', '', $string));
    }
}
