<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Helpers;

class DateParser
{
    /**
     * @param string $date
     * @return \DateTime
     */
    public static function getDateTime(string $date): \DateTime
    {
        $parsedDate = self::parseDate($date);
        
        return new \DateTime($parsedDate);
    }

    /**
     * @param string $date
     * @return string
     */
    public static function parseDate(string $date): string
    {
        return \sprintf('%s-%s', Str::substr($date, 0, 4), Str::substr($date, 4));
    }
}
