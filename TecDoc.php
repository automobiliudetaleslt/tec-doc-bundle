<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle;

final class TecDoc
{
    public const ARTICLE_NUMBER = 0;
    public const OE_NUMBER = 1;
    public const TRADE_NUMBER = 2;
    public const COMPARABLE_NUMBER = 3;
    public const REPLACEMENT_NUMBER = 4;
    public const REPLACED_NUMBER = 5;
    public const EAN_NUMBER = 6;
    public const ANY_NUMBER = 10;

    /**
     * Forbid class initialization.
     */
    private function __construct()
    {
    }
}
