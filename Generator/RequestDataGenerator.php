<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Generator;

use Nfq\Bundle\TecDocBundle\Provider\RequestData\RequestDataProviderInterface;

class RequestDataGenerator
{
    /**
     * @var RequestDataProviderInterface
     */
    private $requestDataProvider;

    /**
     * @param RequestDataProviderInterface $requestDataProvider
     */
    public function __construct(RequestDataProviderInterface $requestDataProvider)
    {
        $this->requestDataProvider = $requestDataProvider;
    }

    /**
     * @param string $method
     * @param array $requestData
     * @param array $requestOptions
     * @return array
     */
    public function generate(string $method, array $requestData, array $requestOptions = []): array
    {
        $requestData = [$method => \array_merge($this->requestDataProvider->getRequestData($method), $requestData)];

        if (!empty($requestOptions)) {
            $requestData['@http'] = $requestOptions;
        }

        return $requestData;
    }
}
