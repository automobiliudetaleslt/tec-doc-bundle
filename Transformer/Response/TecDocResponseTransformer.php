<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Transformer\Response;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformerInterface;
use Nfq\Bundle\TecDocBundle\Exception\TecDocResponseException;
use Nfq\Bundle\TecDocBundle\Model\TecDocResponse;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TecDocResponseTransformer implements ResponseTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transformResponse(
        ResponseInterface $response,
        RequestInterface $request,
        CommandInterface $command,
        DescriptionInterface $description
    ) {
        $responseData = \json_decode((string)$response->getBody(), false, 512, \JSON_THROW_ON_ERROR);

        $tecDocResponse = new TecDocResponse(
            isset($responseData->data->array) ? (array)$responseData->data->array : [],
            (int)$responseData->status
        );

        if (!$tecDocResponse->isSuccessful()) {
            throw new TecDocResponseException(
                $tecDocResponse,
                \sprintf('TecDoc response status: %d', $tecDocResponse->getStatus())
            );
        }

        return $tecDocResponse;
    }
}
